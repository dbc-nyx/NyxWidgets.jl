module Players

using Observables
using Bonito
using Format
import ..Base: NyxAsset, Node, Icon, Button, ToggleButton, Label, lowerdom, dom_id

export AbstractPlayer, Player, PlayerView, isplaying, timestep, timestamp, timestamps,
       timegrid, play, pause, rewind


module Model

using Observables

export AbstractPlayer, Player, isplaying, timestep, timestamp, timestamps, timegrid, play,
       pause, rewind, stepforward, stepbackward, basicloop, speed, toggle, task, interrupted
       #, repeat

"""
    abstract type AbstractPlayer <: AbstractObservable{Int} end

An `AbstractPlayer` gets implemented `Base.min`, `Base.max` and `Observables.observe`,
provided that it implements [`timestep`](@ref) and [`timestamps`](@ref).

The value it stores as an `AbstractObservable` is the time step, which is also an index in
some timestamp array, hence the `Int` type.

Other functions to implement for a featureful player are [`isplaying`](@ref),
[`speed`](@ref) and [`repeat`](@ref).
"""
abstract type AbstractPlayer <: AbstractObservable{Int} end

"""
    Player(times, timestep, speed, isplaying, repeat)
    Player(times; speed=1.0, repeat=false, loop=basicloop, timeout=1, quiet=false)

Head-less player with standard controls.

Timestamp array `times` must be sorted ascendingly.

Play/pause is triggered setting the [`isplaying`](@ref) observable to true/false.
For convenience, see also [`play`](@ref) and [`pause`](@ref).

To use a `Player` as a clock, a listener can be registered on the controller (or
equivalently on [`timestep`](@ref)) using `Observables.on`. For example, a movie animation
would be implemented as follows:

```julia
player = Player(frametimes)

function drawframe(frameindex)
    # ...
end

on(player) do step
    drawframe(step)
end

isplaying(player)[] = true
```

Note that, in the example above, `step` can be 0 if the player is stopped. For more details
about a `Player`'s behavior as an `AbstractObservable`, see [`timestep`](@ref).

!!! note
    If timestamps are originally defined on a regular grid with gaps (missing grid points),
    use the [`timegrid`](@ref) function to create a gap-filled grid of timestamps.

Arguments `loop`, `timeout` and `quiet` are taken by the [`bind`](@ref) function.
"""
struct Player <: AbstractPlayer
    times::AbstractObservable{<:AbstractVector{<:Real}}
    timestep::AbstractObservable{Int}
    speed::AbstractObservable{<:Real}
    isplaying::AbstractObservable{Bool}
    repeat::AbstractObservable{Bool}
    task::Ref{Union{Nothing, Task}}
end

function Player(times, timestep, speed, isplaying, repeat)
    task = Ref{Union{Nothing, Task}}(nothing)
    Player(times, timestep, speed, isplaying, repeat, task)
end

function Player(times; speed=1.0, repeat=false, loop=basicloop, kwargs...)
    if !(times isa AbstractObservable)
        times = Observable(times)
    end
    timestep = Observable(0)
    on(times) do series
        stepmax = length(series)
        # refresh if not in stop state
        step = timestep[]
        if 0 < step
            timestep[] = min(step, stepmax)
        end
    end
    if !(speed isa AbstractObservable)
        speed = Observable(speed)
    end
    on(speed) do v
        0 < v || throw(DomainError(v, "negative or null playback speed"))
    end
    isplaying = Observable(false)
    on(timestep) do step
        if step == 0 && isplaying[]
            isplaying[] = false
        end
    end
    if !(repeat isa AbstractObservable)
        repeat = Observable(repeat)
    end
    player = Player(times, timestep, speed, isplaying, repeat)
    isnothing(loop) || bind(player, loop; kwargs...)
    return player
end

"""
    isplaying(player)

Activity state observable.
"""
isplaying(player) = player.isplaying

"""
    timestep(player)

Time step observable.

Once the player is in a play or pause state, the value in the `timestep` observable is a
valid index.
However, in the stop state, the value is 0. Setting `timestep` to 0 also stops the player.

!!! note
    A player is in the stop state only at the very beginning before playing or stepping
    forward for the first time. It never goes back to the stop state unless programmatically
    reset with `timestep(player)[] = 0` or `rewind(player)`.

"""
timestep(player) = player.timestep

"""
    Base.min(player)

Lower step bound.
"""
Base.min(player::AbstractPlayer) = 1
"""
    Base.max(player)

Upper step bound.
"""
Base.max(player::AbstractPlayer) = length(timestamps(player)[])

"""
    play(player)

Makes a player play, if not already playing.
"""
function play(player)
    ctrl = isplaying(player)
    ctrl[] || (ctrl[] = true)
end

"""
    pause(player; blocking=false)

Makes a player pause, if not already idle. Returns `true`.

If `blocking` is `true`, `pause` waits for the player to stop. If a callback times out and
has to be killed, `pause` returns `false`.
"""
function pause(player; blocking=false)
    ctrl = isplaying(player)
    if ctrl[]
        ctrl[] = false
        if blocking
            try
                wait(player)
            catch
                if interrupted(player)
                    return false
                else
                    rethrow()
                end
            end
        end
    end
    true
end

"""
    Players.toggle(player)

Toggles the [`isplaying`](@ref) observable.
"""
function toggle(player)
    obs = isplaying(player)
    obs[] = !obs[]
end

"""
    Players.task(player)

Returns a `Task` or `nothing`. The task may be complete.
"""
task(player::Player) = player.task[]

task(_) = nothing

task!(player, task) = (player.task[] = task)

"""
    Base.wait(player)

Wait for the player's main loop to stop/pause.

If the player's loop failed, a `TaskFailedException` is thrown.
"""
function Base.wait(player::AbstractPlayer)
    task′= task(player)
    if !(isnothing(task′) || istaskdone(task′))
        wait(task′)
    end
end

"""
    Base.current_exceptions(player; backtrace=true)

Get the stack of exceptions handled by the player's main loop.

Unlike `current_exceptions()`, this method does not have to be called in a `catch` block.
It is more similar in usage to `current_exceptions(task)`.

Example:

```julia
for (exc, bt) in current_exceptions(player)
    showerror(stderr, exc, bt)
    println(stderr)
end
```

"""
function Base.current_exceptions(player::AbstractPlayer; backtrace::Bool=true)
    if isnothing(task(player))
        error("no player tasks found")
    else
        current_exceptions(task(player); backtrace=backtrace)
    end
end

function interrupted(player)
    t = task(player)
    if isnothing(t)
        false
    elseif istaskdone(t)
        exc = current_exceptions(t)
        !isempty(exc) && exc[end][1] isa InterruptException
    else
        false
    end
end

"""
    rewind(player; silent=false)

Reset the time step.

`silent` determines whether to notify the `timestep` observable (false) or not (true).
If the `timestep` observable is not notified, the player is not stopped.
"""
function rewind(player; silent=false)
    step = min(player) - 1
    if silent
        timestep(player).val = step
    else
        timestep(player)[] = step
    end
end

"""
    Players.speed(player)

Playback speed observable.
"""
speed(player) = player.speed

"""
    Players.repeat(player)

Repeat observable.

Its value is true for a looping player, false for a player that pauses or stops at the last
time step.
"""
repeat(player::Player) = player.repeat
repeat(_) = Observable(false)

"""
    timestamps(player)

Timestamp array observable.
"""
timestamps(player) = player.times

"""
    timestamp(player)
    timestamp(player, step)

Timestamp at current or designated step.

A value is returned. No timestamp observable is accessible. To register a listener on the
real time value, use [`timestep`](@ref) and `timestamp` as follows:

```julia
on(timestep(player)) do step
    if step == 0 # stop state
        # clear()
    else
        t = timestamp(player, step)
        # draw(t)
    end
end
```

!!! note
    `on(timestep(player))` can be equivalently written `on(player)`.

"""
timestamp(player, step) = timestamps(player)[][step]
timestamp(player) = timestamp(player, timestep(player)[])

"""
    timegrid(timestamps, Δt=nothing, δt=nothing)

Gap-filled time grid for a regular time series `timestamps` with missing steps (or gaps).

The resulting time series is defined as a range, but the times originally defined in
`timestamps` are substituted to ensure strict equality and avoid numerical errors.

Time interval `Δt` can be specified, otherwise it is inferred as the minimum time difference
between successive timestamps.
Numerical precision `δt` can also be specified, and is otherwise set to a tenth of `Δt`.

Providing `Δt` for long timeseries is highly recommended, to avoid cumulated errors.
"""
function timegrid(times, Δt=nothing, δt=nothing)
    times = unique(sort(times))
    if isnothing(Δt)
        # if times were integers, time step Δt would be the gcd of the differences
        Δt = nextfloat(minimum(diff(times)))
    end
    if isnothing(δt)
        # numerical precision of times
        δt = .1Δt
    end
    timegrid = collect(times[1]:Δt:(times[end]+δt))
    # reinsert the original values to guarantee equality and avoid numerical errors
    i = j = 1
    while i < length(times)
        i += 1
        j += 1
        if times[i] != timegrid[j]
            while true
                Δ = times[i] - timegrid[j]
                # if Δ == 0, equality
                # if Δ < 0 || Δ <= δt, equality with numerical error
                Δ <= δt && break
                j += 1
            end
            timegrid[j] = times[i]
        end
    end
    return timegrid
end

Observables.observe(player::AbstractPlayer) = timestep(player)

const dtmin = 1e-5

"""
    Players.basicloop(player)

Default loop triggered on play, that updates the [`timestep`](@ref) observable on every step
until the [`isplaying`](@ref) observable is false again.

If listeners are synchronous and return after the next expected time step, as many steps as
necessary are skipped so that the player is not delayed.
"""
function basicloop(player)
    stepobs = timestep(player) # step observable
    t = time()
    while isplaying(player)[]
        step = stepobs[] + 1
        if max(player) < step
            if repeat(player)[]
                stepobs[] = min(player)
            else
                isplaying(player)[] = false
            end
        else
            stepobs[] = step
            stepmax = max(player)
            if step < stepmax
                # first, read the state for improved atomicity
                series = timestamps(player)[]
                v = speed(player)[]
                0 < v || throw(DomainError(v, "negative or null playback speed"))
                # estimate elapsed time and target time interval
                t0 = series[step]
                target_dt = (series[step+1] - t0) / v
                @inline elapsed() = time() - t
                # skip steps if necessary
                while target_dt < elapsed()
                    step = stepobs.val += 1 # do not trigger the listeners
                    step < stepmax || break
                    # step < stepmax <= length(series)
                    target_dt = (series[step+1] - t0) / v
                end
                # wait until next step
                remaining_dt = target_dt - elapsed()
                if dtmin <= remaining_dt
                    @debug "Sleeping" dt=remaining_dt
                    sleep(remaining_dt)
                    # TODO: here we assume the scheduler will return control after the
                    # specified sleep duration but is there any guarantee it does so?
                    # Shouldn't we skip steps here as well?
                    # Setting t to t + target_dt instead of time() may help.
                end
                t = t + target_dt
            end
        end
    end
end

"""
    Players.Model.bind(player, loop; timeout=1, quiet=false)

Registers the `loop` function with the [`isplaying`](@ref) observable.

`loop` is run on play (true). `loop` is expected to return on pause/stop (false).
If the associated task is still running after `timeout` seconds, an InterruptException
signal is sent to the task.

When `quiet` is false, timed-out loop events are logged at the warning level, and exceptions
thrown by the loop (other than `InterruptException`) are asynchronously reported on the
standard error stream.
"""
function bind(player::AbstractPlayer, loop; timeout=1, quiet=false)
    on(isplaying(player)) do b
        t = task(player)
        if b
            @debug "Playing"
            @assert isnothing(t) || istaskdone(t) "Should be idle"
            timestep(player)[] == max(player) && rewind(player; silent=true)
            task!(player, Threads.@spawn begin
                try
                    loop(player)
                catch exc
                    pause(player)
                    if !(quiet || exc isa InterruptException)
                        println(stderr)
                        Base.display_error(current_exceptions())
                    end
                    rethrow()
                end
            end)
        else
            @debug "Pause"
            @assert !isnothing(t) "Should be playing"
            @async begin
                sleep(timeout)
                if !istaskdone(t)
                    if quiet
                        @debug "Killing task"
                    else
                        @warn "Killing task"
                    end
                    Base.schedule(t, InterruptException(), error=true)
                end
            end
        end
    end
end

function stepforward(player)
    step = timestep(player)[]
    if step < max(player)
        timestep(player)[] = step + 1
    else
        pause(player)
    end
end

function stepbackward(player)
    step = timestep(player)[]
    if min(player) < step
        timestep(player)[] = step - 1
    else
        pause(player)
    end
end

end # module Model

using .Model
import .Model: repeat


## views

struct TimeSlider
    player
    index
    playpause
    oninput
    attributes
    dom_id
end

function TimeSlider(player::AbstractPlayer, playpause; kwargs...)
    index1 = timestep(player)
    index2 = Observable{Int}(index1[])
    on(index2) do step
        # so weird...
        #step isa Int || @warn "Not an Int" typeof(step)
        # see https://github.com/SimonDanisch/Bonito.jl/issues/249
        index1[] = step # type conversion happens here
    end
    playpause = Observables.observe(playpause)
    oninput = js"""(evt) => {
        var step = evt.srcElement.value;
        console.log(`click on time slider at step ${step}`);
        $index2.notify(parseInt(step));
    }"""
    attributes = Dict{Symbol, Any}(kwargs)
    TimeSlider(player, index2, playpause, oninput, attributes, dom_id())
end

function lowerdom(timeslider::TimeSlider)
    player = timeslider.player
    index1 = timestep(player) # not timeslider.index!
    oninput = timeslider.oninput
    attributes = timeslider.attributes
    return DOM.input(; type="range", min=1, max=max(player), value=index1, step=1,
                     oninput=oninput, attributes...)
end

function Bonito.jsrender(session::Session, slider::TimeSlider; kwargs...)
    node = lowerdom(session, slider)
    #
    player = slider.player
    on(session, timestamps(player)) do series
        stepmax = length(series)
        evaljs(session, js"$node.max = $stepmax;")
    end
    #
    playpause = slider.playpause
    currentstep = max(1, timestep(player)[])
    Bonito.onload(session, node, js"""(slider) => {
        slider.value = $currentstep;
        slider.addEventListener('keyup', (evt) => {
            if (evt.keyCode == 32) {
                console.log('space bar pressed');
                $playpause.notify(true);
            }
        })
    }""")
    #
    return node
end

# Playback speed selector

function discretespeed(backspeed, frontspeeds, speedbounds)
    for (i, speedbound) in enumerate(speedbounds)
        if backspeed < speedbound
            return frontspeeds[i]
        end
    end
    return frontspeeds[end]
end

struct SpeedSelector
    frontspeeds
    speedbounds
    frontspeed
    backspeed
    onchange
    attributes
    dom_id
end

function SpeedSelector(player, playbackspeeds; kwargs...)
    frontspeeds = playbackspeeds
    speeds = [parse(Float64, speed) for speed in frontspeeds]
    issorted(speeds) || error("Playback speeds must be ascendingly sorted")
    1.0 in speeds || error("Default playback speed (1.0) is expected")
    speedbounds = @. (speeds[1:end-1] + speeds[2:end]) / 2
    #
    backspeed = speed(player)
    frontspeed = Observable("")
    on(frontspeed) do speed
        backspeed[] = parse(Float64, speed)
    end
    #
    onchange = js"(evt)=>{ $frontspeed.notify(evt.srcElement.value); }"
    attributes = Dict{Symbol, Any}(kwargs)
    SpeedSelector(frontspeeds, speedbounds, frontspeed, backspeed, onchange, attributes,
                  dom_id())
end

function lowerdom(selector::SpeedSelector)
    frontspeeds, onchange = selector.frontspeeds, selector.onchange
    return DOM.select(DOM.option(speed; value=speed) for speed in frontspeeds;
                      onchange=onchange, selector.attributes...)
end

function Bonito.jsrender(session::Session, selector::SpeedSelector)
    node = lowerdom(session, selector)
    backspeed = selector.backspeed
    on(session, backspeed) do speed
        speed = discretespeed(speed, selector.frontspeeds, selector.speedbounds)
        evaljs(session, js"$node.value = $speed;")
    end
    notify(backspeed)
    return node
end

"""
    PlayerView(player, playpause, stepforward, stepbackward, repeat,
        stepforwardtext, stepbackwardtext, speedselector, timeslider, timelabel)
    PlayerView(player, playbackspeeds=nothing;
        playpausetext="Play / pause\\n[space]",
        stepforwardtext="One step forward\\n[right arrow]",
        stepbackwardtext="One step backward\\n[left arrow]",
        repeattext="Repeat",
        labeldecimals=2)

Player view that implements `Bonito.jsrender`.

If playback speeds are specified as an array of numeric strings (*e.g.* "1.0"), an
additional dropdown menu is displayed between the repeat button and the slider.
"""
struct PlayerView
    player::AbstractPlayer
    playpause::ToggleButton
    stepforward::Button
    stepbackward::Button
    repeat::ToggleButton
    speedselector::Union{Nothing, SpeedSelector}
    timeslider::TimeSlider
    timelabel::Label
    attributes
    dom_id
end

function PlayerView(player::AbstractPlayer, playbackspeeds=nothing;
        playpausetext="Play / pause\n[space]",
        stepforwardtext="One step forward\n[right arrow]",
        stepbackwardtext="One step backward\n[left arrow]",
        repeattext="Repeat",
        labeldecimals=2,
        attributes...)
    playpause = ToggleButton(Icon("nyx-play-pause-icon"), isplaying(player);
                             class="nyx-icon-button", title=playpausetext)
    #
    stepforward = Observable(false)
    on(stepforward) do _
        Players.stepforward(player)
    end
    stepforward = Button(Icon("nyx-step-forward-icon"), stepforward;
                         class="nyx-icon-button", title=stepforwardtext)
    #
    stepbackward = Observable(false)
    on(stepbackward) do _
        Players.stepbackward(player)
    end
    stepbackward = Button(Icon("nyx-step-backward-icon"), stepbackward;
                          class="nyx-icon-button", title=stepbackwardtext)
    #
    repeat = ToggleButton(Icon("nyx-repeat-icon"), Players.repeat(player);
                          class="nyx-icon-button", title=repeattext)
    #
    speedselector = if !isnothing(playbackspeeds)
        SpeedSelector(player, playbackspeeds)
    end
    #
    timeslider = TimeSlider(player, playpause.button)
    #
    fmt = FormatExpr("t={: .$(labeldecimals)f} s")
    label = map(timestep(player)) do step
        t = timestamp(player, max(1, step))
        format(fmt, t)
    end
    timelabel = Label(label)
    #
    attributes = Dict{Symbol, Any}(attributes)
    PlayerView(player, playpause, stepforward, stepbackward, repeat, speedselector,
               timeslider, timelabel, attributes, dom_id())
end

function lowerdom(player::PlayerView)
    children = Any[player.playpause, player.stepbackward, player.stepforward, player.repeat,
                   player.timeslider, player.timelabel]
    if !isnothing(player.speedselector)
        insert!(children, 5, player.speedselector)
    end
    return DOM.div(children; class="nyx-player", player.attributes...)
end

function Bonito.jsrender(session::Session, player::PlayerView)
    Bonito.jsrender(session, NyxAsset(:css))
    Bonito.jsrender(session, NyxAsset(:theme))
    Bonito.jsrender(session, NyxAsset(:iconset))
    node = lowerdom(session, player)
    Bonito.onload(session, node, js"""(node) => {
        var slider = node.getElementsByTagName('input')[0];
        node.addEventListener('mouseenter', ()=>{ slider.focus(); })
    }""")
    return node
end

function Bonito.jsrender(session::Session, player::AbstractPlayer)
    Bonito.jsrender(session, PlayerView(player))
end

"""
    Players.bind(session, container)

Binds the arrow keys of the keyboard to the time slider every time the mouse enters the area
of the `container` element.

Element `container` must have a player widget as a child (with class name *nyx-player*).

The [`NyxJS`](@ref Main.NyxWidgets.Base.NyxJS) asset is required.
"""
function bind(session::Session, container::Node)
    Bonito.onload(session, container, js"""(container) => {
        NyxWidgets.focusOnTimeSlider(container);
        function listener() {
            NyxWidgets.focusOnTimeSlider(container);
        }
        $container.addEventListener('mouseenter', listener);
    }""")
end

end
