module FilePickers

using Observables
using Bonito
import ..Base: dom_id, lowerdom

export FilePicker, pickfile, uploadedfile

"""
    FilePicker(; multiple=true)

File picker utility.

Like other widgets, it needs to be rendered as part of the DOM. However, it is hidden in the
webpage. On calling `pickfile`, the browser opens up a file browser window.

File content can be received listening to the observable returned by [`uploadedfile`](@ref).
This observable is notified for each new file; therefore notified as many times as selected
files.

For convenience, `FilePicker` exposes two event observables:
* attribute `open`: notified when the client's file browser opens up;
* attribute `completion`: notified once all the selected files are transferred; this can be
  useful when multiple files can be selected at once.

Note that no events are available for the case the user closes the file browser with the
*Cancel* button.
"""
struct FilePicker
    uploadedfile::AbstractObservable{Dict{String, String}}
    open::AbstractObservable{Bool}
    completion::AbstractObservable{Bool}
    multiple::Bool
    dom_id
end

function FilePicker(; multiple=true)
    uploadedfile = Observable(Dict{String, String}())
    open = Observable(false)
    completion = Observable(false)
    FilePicker(uploadedfile, open, completion, multiple, dom_id())
end

Observables.observe(picker::FilePicker) = Observables.observe(picker.uploadedfile)
Observables.on(f::Function, picker::FilePicker) = on(f, picker.uploadedfile)

function lowerdom(picker::FilePicker)
    DOM.input(; type="file",
        multiple=picker.multiple,
        style="display: none;",
        onchange=js"""(event) => {
        const files = event.target.files; // shorter alias
        if (files) {
            const obs = $(picker.uploadedfile); // shorter alias
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                //console.log('Uploading file: %s', file.name);
                const reader = new FileReader();
                if (i + 1 < files.length) {
                    reader.onload = (e) => {
                        obs.notify({'name': file.name, 'content': e.target.result});
                    };
                } else {
                    reader.onload = (e) => {
                        obs.notify({'name': file.name, 'content': e.target.result});
                        //console.log('Transfer complete');
                        $(picker.completion).notify(true);
                    };
                }
                reader.readAsText(file);
            }
        }}""")
end

function Bonito.jsrender(session::Session, picker::FilePicker)
    node = lowerdom(session, picker)
    onjs(session, picker.open, js"(b) => { if (b) { $(node).click(); } }")
    return node
end

"""
    pickfile(filepicker)

Makes the client web browser display a file browser so that the user can select files.
"""
function pickfile(picker::FilePicker)
    picker.open[] = true
end

"""
    uploadedfile(filepicker)

`Dict{String, String}` observable that is notified whenever a file has been uploaded from
the client browser. The keys are *name* and *content*. The corresponding values are the file
name and content (as text) respectively.

A straight-forward implementation of a file writer is:
```julia
on(uploadedfile(filepicker)) do fileinfo
    open(fileinfo["name"], "w") do f
        write(f, fileinfo["content"])
    end
end
```
"""
uploadedfile(picker::FilePicker) = picker.uploadedfile

end
