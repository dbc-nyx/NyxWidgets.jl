module Muscles

using Bonito
using ..Graphics
import ..Graphics: children
import ..Base: NyxAsset, Node, lowerdom, dom_id
using LazyArtifacts
using Observables

export Muscle, MuscleWidget, musclename, MultiSegmentMuscleWidget, segments, muscles

const __shortnames__ = Set(("DA1", "DA2","DA3", "DO1", "DO2", "DO3", "DO4", "DO5", "DT1",
                            "VA1", "VA2", "VA3", "VO1", "VO2", "VO3", "VO4", "VO5", "VO6",
                            "VT1", "VL1", "VL2", "VL3", "VL4", "LT1", "LT2", "LT3", "LT4",
                            "LL1", "LO1", "SBM"))

"""
    Muscle(name, segment, side)
    Muscle(name; segment=nothing, side=nothing)

Muscle locator.

The short name is used (*e.g.* *DA1*).
`segment` can be a segment name (*e.g.* *A2*) or an `AbstractObservable`.
`side` can be *left*, *right* or an `AbstractObservable`.
"""
struct Muscle
    name
    segment
    side
    function Muscle(name, segment, side)
        name in __shortnames__ || throw(DomainError(name, "unknown muscle name"))
        side′= Observables.to_value(side)
        __sides__ = ("left", "right", nothing)
        side′ in __sides__ || throw(DomainError(side′, "not left or right"))
        new(name, segment, side)
    end
end

Muscle(name; segment=nothing, side=nothing) = Muscle(name, segment, side)

function Base.:(==)(m1::Muscle, m2::Muscle)
    se1 = Observables.to_value(m1.segment)
    se2 = Observables.to_value(m2.segment)
    si1 = Observables.to_value(m1.side)
    si2 = Observables.to_value(m2.side)
    return m1.name == m2.name &&
        (isnothing(se1) || isnothing(se2) || se1 == se2) &&
        (isnothing(si1) || isnothing(si2) || si1 == si2)
end

function loadpolygons(jsonfile)
    polygons = Dict{String,Tuple{Vector{Vector{Float64}}, Vector{Float64}}}()
    lines = readlines(jsonfile)
    l = 1
    @assert startswith(lines[l], "{")
    while true
        l += 1
        line = lines[l]
        startswith(line, "}") && break
        _, label, _ = split(line, '"')
        l += 1
        line = lines[l]
        _, elem, _, coords, _ = split(line, '"')
        @assert elem == "points"
        coords = [[parse(Float64, c) for c in split(p, ',')] for p in split(coords)]
        l += 1
        line = lines[l]
        _, elem, _, labelpos, _ = split(line, '"')
        @assert elem == "label"
        labelpos = [parse(Float64, c) for c in split(labelpos, ',')]
        l += 1
        polygons[string(label)] = (coords, labelpos)
    end
    return polygons
end

const __muscleposition__ = Dict(:D=>"dorsal", :V=>"ventral", :L=>"lateral")
const __muscleorientation__ = Dict(:A=>"acute", :L=>"longitudinal", :O=>"oblique",
                                   :T=>"transverse")

function musclename(short)
    if short == "SBM"
        "segment border muscle"
    else
        try
            position = __muscleposition__[Symbol(short[1])]
            orientation = __muscleorientation__[Symbol(short[2])]
            index = short[3:end]
            join((position, orientation, "muscle", index), " ")
        catch
            ""
        end
    end
end

"""
    MuscleWidget([path]; ...)
    MuscleWidget(polygons; segment="A2", viewBox="0 0 257 500",
                 musclelabel=true, segmentlabel=false, segmentselector=false)

SVG widget with dynamically-stylable polygons and optional labels.
Polygons can be specified as a dictionnary of *name => (path, labelpos)* pairs with *path* a
vector of 2-element vectors and *labelpos* a 2-element vector.

Alternatively, the positional data can be loaded from a JSON file. Per default, an artifact
is downloaded and represents *Drosophila* larval muscles adapted from
[Landgraf *et al.* 2003](https://doi.org/10.1371/journal.pbio.0000041.g001).

If `segment` is *A1* and `segmentselector` is false, muscle *VT1* is excluded.
"""
struct MuscleWidget
    segment
    side
    upsidedown
    traces
    svg
    segmentlabel
    segmentselector
    class
    attributes
    dom_id
end

function musclewidget(W::Type, path::AbstractString=artifact"muscles"; kwargs...)
    if isdir(path)
        paths = readdir(path; join=true)
        path = paths[1]
        @debug "Loading file" path
    end
    polygons = loadpolygons(path)
    W(polygons; kwargs...)
end

MuscleWidget(args...; kwargs...) = musclewidget(MuscleWidget, args...; kwargs...)

function MuscleWidget(polygons::AbstractDict;
        segment="A2", side=nothing, viewBox="0 0 257 500", musclelabel=true,
        segmentlabel=false, segmentselector=false, upsidedown=false, class=nothing,
        attributes...)
    attrs = Dict{Symbol, String}()
    if upsidedown
        attrs[:transform] = "scale(1,-1)"
    end
    if segmentselector && segmentlabel
        segmentlabel = false
    end
    traces = Dict{String, Graphics.G}()
    for (label, (polygon, labelpos)) in pairs(polygons)
        if !segmentselector
            segment == "A1" && label == "VT1" && continue
        end
        x, y = labelpos
        if upsidedown
            y = -y
        end
        trace = Graphics.G([
            Graphics.Title(musclename(label)),
            Graphics.Polygon(polygon),
            Graphics.Text(label; x=x, y=y, attrs...),
        ])
        musclelabel || pop!(trace.children)
        traces[label] = trace
        # base listeners
        Graphics.active(trace)[] = false
    end
    if segmentselector && !(segment isa AbstractObservable)
        segment = Observable(segment)
    end
    baseclass = "nyx-segment"
    svg = Graphics.SVG(collect(values(traces));
                       hoverable=true, viewBox=viewBox, class=baseclass, attrs...)
    class = isnothing(class) ? baseclass : join((baseclass, class), " ")
    attributes = Dict{Symbol, Any}(attributes)
    MuscleWidget(segment, side, upsidedown, traces, svg, segmentlabel, segmentselector,
                 class, attributes, dom_id())
end

function lowerdom(widget::MuscleWidget)
    dom = Dict{Symbol, Node}()
    #
    elements = Any[widget.svg]
    #
    segment = widget.segment
    if widget.segmentlabel
        segmentlabel = DOM.label(segment)
        push!(elements, DOM.div(segmentlabel))
        dom[:segmentlabel] = segmentlabel
    end
    #
    if widget.segmentselector
        @assert segment isa AbstractObservable
        noVT1 = Graphics.hidden(widget.traces["VT1"])
        on(segment) do segment
            @debug "Selecting segment" segment
            if segment == "A1"
                noVT1[] || (noVT1[] = true)
            else
                noVT1[] && (noVT1[] = false)
            end
        end
        segmentselector = DOM.select(
            DOM.option("A1"; value="A1"),
            DOM.option("A2"; value="A2"),
            DOM.option("A3"; value="A3"),
            DOM.option("A4"; value="A4"),
            DOM.option("A5"; value="A5"),
            DOM.option("A6"; value="A6");
            onchange=js"(event)=>{ $segment.notify(event.target.value); }",
        )
        push!(elements, DOM.div(segmentselector))
        dom[:segmentselector] = segmentselector
    end
    if widget.upsidedown
        reverse!(elements)
    end
    dom[:node] = DOM.div(elements; class=widget.class, widget.attributes...)
    return dom
end

function Bonito.jsrender(session::Session, widget::MuscleWidget)
    Bonito.jsrender(session, NyxAsset(:css))
    Bonito.jsrender(session, NyxAsset(:theme))
    dom = lowerdom(session, widget)
    node = dom[:node]
    segment = widget.segment
    if segment isa AbstractObservable
        segmentselector = dom[:segmentselector]
        # at this point, Bonito.render_node has mutated segmentselector which now has a
        # data-jscall-is attribute
        Bonito.onload(session, segmentselector, js"(node)=>{ node.value = $(segment[]); }")
        notify(segment) # show or hide the VT1 muscle
    end
    return node
end

Base.getindex(widget::MuscleWidget, muscle::AbstractString) = widget.traces[muscle]

Base.haskey(widget::MuscleWidget, muscle::AbstractString) = haskey(widget.traces, muscle)

function Muscle(muscle, widget::MuscleWidget)
    Muscle(muscle, widget.segment, widget.side)
end

function Base.getindex(widget::MuscleWidget, muscle::Muscle)
    Muscle(muscle.name, widget.segment, widget.side) == muscle || throw(KeyError(muscle))
    return widget[muscle.name]
end

function Base.haskey(widget::MuscleWidget, muscle::Muscle)
    haskey(widget, muscle.name) && \
        Muscle(muscle.name, widget.segment, widget.side) == muscle
end

polygon(g::Graphics.G) = g.children[2]

function Graphics.opacity(widget::MuscleWidget, muscle)
    Graphics.opacity(polygon(widget[muscle]))
end

function Graphics.color(widget::MuscleWidget, muscle; kwargs...)
    Graphics.color(polygon(widget[muscle]); kwargs...)
end

function muscles(widget::MuscleWidget)
    [(Muscle(name, widget.segment, widget.side), elem)
     for (name, elem) in pairs(widget.traces)]
end

segments(widget::MuscleWidget) = [widget]


"""
    MultiSegmentMuscleWidget([path]; ...)
    MultiSegmentMuscleWidget(polygons; segments=("A1", "A2", "A3", "A4", "A5", "A6"),
                             sides=("right", "left"), shift=16, width=20, ...)

SVG widget showing -- per default -- the muscles of 6 abdominal segments of a *Drosophila*
larva, both left and right sides around the dorsal spine, seen from outside.

It wraps as many [`MuscleWidget`](@ref) objects as specified segments.
Trailing keyword arguments are passed to each `MuscleWidget` constructor.
"""
struct MultiSegmentMuscleWidget
    segments::Vector{MuscleWidget}
    clickedsegment::Observable{Int}
    sidelabel::Union{Bool, Symbol}
    attributes
    dom_id
end

function MultiSegmentMuscleWidget(polygons::AbstractDict;
        segments=("A1","A2","A3","A4","A5","A6"), sides=("right", "left"),
        shift=16, width=20, style=nothing, sidelabel=false, kwargs...)
    function segmentstyle(i)
        if i == 1
            "width: $(width)%;"
        else
            "position: absolute; top: 0; left: $((i - 1) * shift)%; width: $(width)%;"
        end
    end
    clickedsegment = Observable{Int}(0)
    n = length(segments)
    segments = [MuscleWidget(polygons;
                             side=side,
                             segment=seg,
                             segmentlabel=true,
                             segmentselector=false,
                             upsidedown=side=="right",
                             style=segmentstyle(i),
                             onclick=js"""()=>{
                                $clickedsegment.notify($(n * (j - 1) + i));
                             }""",
                             kwargs...)
                for (j, side) in enumerate(sides) for (i, seg) in enumerate(segments)]
    attributes = Dict{Symbol, Any}()
    if !isnothing(style)
        attributes[:style] = style
    end
    MultiSegmentMuscleWidget(segments, clickedsegment, sidelabel, attributes, dom_id())
end

function MultiSegmentMuscleWidget(args...; kwargs...)
    musclewidget(MultiSegmentMuscleWidget, args...; kwargs...)
end

function lowerdom(widget::MultiSegmentMuscleWidget)
    segments_per_side = Vector{MuscleWidget}[]
    sided_segments = nothing
    current_side = nothing
    for segment in widget.segments
        @assert !isnothing(segment.side)
        if segment.side != current_side
            sided_segments = MuscleWidget[]
            push!(segments_per_side, sided_segments)
            current_side = segment.side
        end
        push!(sided_segments, segment)
    end
    function makeside(sided_segments)
        div = DOM.div(sided_segments; class="nyx-segments")
        if widget.sidelabel !== false
            sidelabel_side = widget.sidelabel === true ? :true : widget.sidelabel
            sidelabel_class = "nyx-segment-side-label-$sidelabel_side"
            sidelabel = DOM.span(sided_segments[1].side; class=sidelabel_class)
            div = if sidelabel_side === :right
                DOM.div(div, sidelabel)
            else
                DOM.div(sidelabel, div)
            end
        end
        return div
    end
    return DOM.div(makeside(sided_segments)
                   for sided_segments in segments_per_side; class="nyx-segment-halves",
                   widget.attributes...)
end

function Bonito.jsrender(session::Session, widget::MultiSegmentMuscleWidget)
    return lowerdom(session, widget)
end

segments(widget::MultiSegmentMuscleWidget) = widget.segments

muscles(widget::MultiSegmentMuscleWidget) = Iterators.flatmap(muscles, widget.segments)

"""
    click(widget)
    click(Muscle, widget)

Observable notified on each click on a muscle mesh.
It stores either `nothing` or a pair of a muscle locator (type [`Muscle`](@ref)) and a mesh
(type [`Graphics.G`](@ref Main.NyxWidgets.Graphics.G)).

`click` instantiates the observable. Consequently, the latter should be reused instead of
calling `click` multiple times.
"""
click(widget) = click(Muscle, widget)

function click(::Type{Muscle}, widget)
    T = Tuple{Muscle, Graphics.G}
    clickedmuscle = Observable{Union{Nothing, T}}(nothing)
    for (muscle, mesh) in muscles(widget)
        on(Graphics.click(mesh)) do _
            clickedmuscle[] = (muscle, mesh)
        end
    end
    return clickedmuscle
end

"""
    click(MuscleWidget, widget)

Observable notified on each click on the bounding box of a segment.
It stores either `nothing` or a [`MuscleWidget`](@ref).

`click` instantiates the observable. Consequently, the latter should be reused instead of
calling `click` multiple times.

If both `click(Muscle, widget)` (or `click(widget)`)  and `click(MuscleWidget, widget)` are
registered, both are triggered on a click on a muscle mesh.
"""
function click(::Type{MuscleWidget}, widget)
    clickedsegment = Observable{Union{Nothing, MuscleWidget}}(nothing)
    on(widget.clickedsegment) do k
        if 0 < k
            clickedsegment[] = widget.segments[k]
        end
    end
    return clickedsegment
end

end
