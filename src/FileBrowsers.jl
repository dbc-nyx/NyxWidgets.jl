module FileBrowsers

using Observables
using Bonito
import ..Base: NyxAsset, register, lowerdom, dom_id, dom_id!
using ..FilePickers

export FileBrowser, FileBrowserView, FileBrowserSettings, supports_upload, supports_reload,
       supports_download, supports_create_directory, selectfile, selectedfile, uploadedfile,
       workingdir


module Model

using Observables

export FileBrowser, selectfile, selectedfile, workingdir, formatpath, dircontent

struct FileBrowser
    root::String
    workingdir::AbstractObservable{AbstractString}
    fileselector::Function
    content::AbstractObservable{Vector{Tuple{Bool, AbstractString}}}
    selectedfile::AbstractObservable{AbstractString}
    download_request::AbstractObservable{AbstractString}
    download_ready::AbstractObservable{Tuple{AbstractString, AbstractString}}
    newdirectory::AbstractObservable{AbstractString}
    delete::AbstractObservable{AbstractString}
end

function FileBrowser(fileselector::Function,
        root::Union{Nothing, String}=nothing,
        prepare_download::Union{Nothing, Function}=nothing)
    if isnothing(root)
        root = Sys.iswindows() ? "" : "/"
    end
    workingdir = Observable{AbstractString}("")
    content = Observable(Tuple{Bool, AbstractString}[])
    on(workingdir) do dir
        isdir(dir) || throw("Not a directory: $dir")
        dir = normpath(dir)
        if dir[end] in ('/', '\\')
            # note: dirname("/") == "/"
            dir = dirname(dir)
        end
        workingdir.val = dir
        entries = [(isdir(joinpath(dir, entry)), entry)
                   for entry in readdir(dir) if fileselector(dir, entry)]
        if dir != root
            pushfirst!(entries, (true, ".."))
        end
        content[] = entries
    end
    selectedfile = Observable{AbstractString}("")
    download_request = Observable{AbstractString}("")
    download_ready = Observable{Tuple{AbstractString, AbstractString}}(("", ""))
    on(download_ready) do data
        route, filename = data
        @info "Download ready" route filename
    end
    on(download_request) do filename
        srcfile = joinpath(workingdir[], filename)
        if isfile(srcfile)
            route = if isnothing(prepare_download)
                if Sys.iswindows()
                    @assert '\\' ∉ srcfile "not implemented"
                end
                "file://$srcfile"
            else
                prepare_download(srcfile)
            end
            if !isnothing(route)
                download_ready[] = (route, filename)
            end
        elseif isdir(srcfile)
            @error "Directory transfer not implemented"
        else
            @error "File not found" srcfile
        end
    end
    newdirectory = Observable{AbstractString}("")
    on(newdirectory) do dirname
        if valid_directory_name(dirname)
            dirpath = joinpath(workingdir[], dirname)
            if ispath(dirpath)
                @warn "Directory (or file) already exists" dirpath
            else
                @info "Creating directory" dirpath
                mkpath(dirpath)
                notify(workingdir) # refresh directory content
            end
        else
            @warn "Rejecting directory name" dirname
        end
    end
    delete = Observable{AbstractString}("")
    on(delete) do entry
        isempty(entry) && return
        # note: the controller is in charge of collecting user confirmation for non-empty
        #       directories
        entry = joinpath(workingdir[], entry)
        if ispath(entry)
            rm(entry; recursive=true)
            notify(workingdir) # refresh directory content
        end
    end
    FileBrowser(root, workingdir, fileselector, content, selectedfile, download_request,
                download_ready, newdirectory, delete)
end

function FileBrowser(root=nothing; showhidden=false, prepare_download=nothing, kwargs...)
    if showhidden
        FileBrowser((_, _) -> true, root, prepare_download; kwargs...)
    else
        FileBrowser(root, prepare_download; kwargs...) do _, entry
            entry[1] != '.'
        end
    end
end

"""
    workingdir(filebrowser)

Access the observable notified on changes in working directory.

Listeners receive raw path values as input argument, which may differ from the validated
value stored in the observable. To access the validated value:

```julia
wd = workingdir(filebrowser)
on(wd) do _
    dir = wd[]
    # do something with `dir` instead of the (discarded) input argument
end
```
"""
workingdir(fb::FileBrowser) = fb.workingdir

dircontent(fb::FileBrowser) = fb.content

"""
    selectedfile(filebrowser)

Access the observable notified on file selections.

Listeners receive absolute paths as input argument.
"""
selectedfile(fb::FileBrowser) = fb.selectedfile

function valid(fb::FileBrowser, entry)
    entry ∈ [entry for (_, entry) in fb.content[]]
end

function valid_directory_name(entry)
    if !all(isprint, entry) # includes empty strings
        return false
    elseif startswith(entry, '.') # do not allow hidden entries (or . and ..)
        return false
    elseif endswith(entry, '.') || endswith(entry, ' ') # Windows rule; enforce on all OSes
        return false
    else
        for forbidden_char in "/\\<>:\"|?*" # forbidden printable chars on Unix or Windows
            if forbidden_char in entry
                return false
            end
        end
    end
    return true
end

function selectfile(fb::FileBrowser, dst::AbstractString)
    if isempty(fb.workingdir[])
        isabspath(dst) || throw("Initial destination should be a valid absolute path")
        if !isempty(fb.root) && startswith(relpath(dst, fb.root), "..")
            throw("Initial destination is not below the root path")
        end
        if isfile(dst)
            dir, file = splitdir(dst)
            fb.workingdir[] = dir
            if valid(fb, file) # or fb.fileselector(dir, file)
                fb.selectedfile[] = dst
            else
                throw("Invalid destination: $file")
            end
        else
            fb.workingdir[] = dst
        end
    elseif valid(fb, dst)
        if dst == ".."
            fb.workingdir[] = dirname(fb.workingdir[])
        else
            dst = joinpath(fb.workingdir[], dst)
            if isfile(dst)
                fb.selectedfile[] = dst
            elseif isdir(dst)
                fb.workingdir[] = dst
            else
                throw("Entry type not supported: $dst")
            end
        end
    else
        throw("Invalid destination: $dst")
    end
end

Base.isready(browser::FileBrowser) = !isempty(browser.workingdir[])

const ROOT = Sys.iswindows() ? "\\" : "/"

function formatpath(path, root, maxpathlength)
    parts = splitpath(relpath(path, root))
    pushfirst!(parts, ROOT)
    k = length(parts)
    minpath = parts[k]
    while 1 < k
        k -= 1
        path = joinpath(parts[k], minpath)
        if length(path) <= maxpathlength
            minpath = path
        else
            break
        end
    end
    if !startswith(minpath, ROOT)
        minpath = joinpath("...", minpath)
    end
    return minpath
end

end # module Model

import .Model: Model, selectfile, selectedfile, workingdir, formatpath, dircontent, ROOT


module View

import ...Base: Button, Icon, dom_id, lowerdom
using Observables
using Bonito

export FileBrowser, selectoption, maxpathlength, supports_upload, supports_reload,
       supports_download, supports_create_directory, supports_delete, hascontrols

struct FileBrowser
    # observables
    workingdir::AbstractObservable{String}
    selectoption::AbstractObservable{String}
    reload::Union{Nothing, Button}
    upload::Union{Nothing, Button}
    download::Union{Nothing, Button}
    createdir::Union{Nothing, Button}
    delete::Union{Nothing, Button}
    hovered_entry::AbstractObservable{String}
    dircontent::AbstractObservable
    # settings
    dircontent_rows
    maxpathlength
    # DOM tracker
    dom_id
end

function FileBrowser(settings)
    FileBrowser(; reload_button=settings.reload_button,
                reload_button_title=settings.reload_button_title,
                upload_button=settings.upload_button,
                upload_button_title=settings.upload_button_title,
                maxpathlength=settings.maxpathlength,
                dircontent_rows=settings.dircontent_rows)
end

function FileBrowser(; reload_button=true, reload_button_title="Reload",
        upload_button=false, upload_button_title="Upload",
        download_button=false, download_button_title="Download",
        create_directory_button=false, create_directory_button_title="New directory",
        delete_button=false, delete_button_title="Delete",
        maxpathlength= buttons -> 72 - buttons * 6 , dircontent_rows=5)
    workingdir = Observable("")
    dircontent = Observable(Tuple{String, String}[])
    #
    if maxpathlength isa Function
        buttons = reload_button + upload_button
        maxpathlength = maxpathlength(buttons)
    end
    reload = if reload_button
        Button(Icon("nyx-reload-icon"), Observable(false);
               class="nyx-icon-button", title=reload_button_title)
    end
    upload = if upload_button
        Button(Icon("nyx-upload-icon"), Observable(false);
               class="nyx-icon-button", title=upload_button_title)
    end
    download = if download_button
        Button(Icon("nyx-download-icon"), Observable(false);
               class="nyx-icon-button", title=download_button_title)
    end
    createdir = if create_directory_button
        Button(Icon("nyx-new-directory-icon"), Observable(false);
               class="nyx-icon-button", title=create_directory_button_title)
    end
    delete = if delete_button
        Button(Icon("nyx-delete-icon"), Observable(false);
               class="nyx-icon-button", title=delete_button_title)
    end
    hovered_entry = Observable("")
    on(hovered_entry) do entry
        @info "Hovered entry" entry
    end
    #
    FileBrowser(workingdir,
        Observable(""),
        reload,
        upload,
        download,
        createdir,
        delete,
        hovered_entry,
        dircontent,
        dircontent_rows,
        maxpathlength,
        dom_id())
end

function lowerdom(fb::FileBrowser)
    dom = Dict{Symbol, Any}()
    workingdir_dom = DOM.input(value=fb.workingdir, type="text", readonly=true)
    navbar_elements = [workingdir_dom, fb.reload, fb.createdir, fb.upload]
    navbar_dom = DOM.div(el for el in navbar_elements if !isnothing(el);
                         class="nyx-filebrowser-navbar")
    dircontent_dom = DOM.select(; size=fb.dircontent_rows,
                                class="nyx-filebrowser-dircontent")
    node_children = [navbar_dom, dircontent_dom]
    if hascontrols(fb)
        entrycontrols = [fb.delete, fb.download]
        entrycontrols_dom = DOM.div(el for el in entrycontrols if !isnothing(el);
                                    class="nyx-filebrowser-entrycontrols",
                                    style="display:none; position:absolute;")
        push!(node_children, entrycontrols_dom)
        dom[:entrycontrols] = entrycontrols_dom
    end
    node = DOM.div(node_children; class="nyx-filebrowser")
    dom[:workingdir] = workingdir_dom
    dom[:navbar] = navbar_dom
    dom[:dircontent] = dircontent_dom
    dom[:node] = node
    return dom
end

selectoption(view::FileBrowser) = view.selectoption
maxpathlength(view::FileBrowser) = view.maxpathlength
supports_reload(view::FileBrowser) = !isnothing(view.reload)
supports_upload(view::FileBrowser) = !isnothing(view.upload)
supports_download(view::FileBrowser) = !isnothing(view.download)
supports_create_directory(view::FileBrowser) = !isnothing(view.createdir)
supports_delete(view::FileBrowser) = !isnothing(view.delete)
function hascontrols(view::FileBrowser)
    supports_download(view) || supports_delete(view::FileBrowser)
end

end # module View

import .View: View, selectoption, maxpathlength, supports_upload, supports_reload,
              supports_download, supports_create_directory, supports_delete, hascontrols

const FileBrowserView = View.FileBrowser


## legacy code

"""
    FileBrowserSettings(
        root::Union{AbstractString, Nothing},
        reload_button::Bool,
        reload_button_title::AbstractString,
        upload_button::Bool,
        upload_button_title::AbstractString,
        maxpathlength::Union{Number, Function},
        dircontent_rows::Integer,
    FileBrowserSettings(root=nothing;
        reload_button=true,
        reload_button_title="Reload",
        upload_button=false,
        upload_button_title="Upload",
        maxpathlength= buttons -> 72 - buttons * 6 ,
        dircontent_rows=5)

Settings for [`FileBrowser`](@ref) objects.

FileBrowserSettings is deprecated.
The same arguments to FileBrowserSettings can now be passed directly to FileBrowser.
"""
mutable struct FileBrowserSettings
    root::Union{AbstractString, Nothing}
    reload_button::Bool
    reload_button_title::AbstractString
    upload_button::Bool
    upload_button_title::AbstractString
    maxpathlength::Union{Number, Function}
    dircontent_rows::Integer
end

function FileBrowserSettings(root=nothing;
        reload_button=true,
        reload_button_title="Reload",
        upload_button=false,
        upload_button_title="Upload",
        maxpathlength= buttons -> 72 - buttons * 6 ,
        dircontent_rows=5,
    )
    @warn "FileBrowserSettings is deprecated; pass the arguments directly to FileBrowser"
    FileBrowserSettings(root,
        reload_button,
        reload_button_title,
        upload_button,
        upload_button_title,
        maxpathlength,
        dircontent_rows,
    )
end

View.supports_reload(settings::FileBrowserSettings) = settings.reload_button
View.supports_upload(settings::FileBrowserSettings) = settings.upload_button

## end legacy code


"""
    FileBrowser(dir=nothing;
        root=nothing;
        reload_button=true,
        reload_button_title="Reload",
        upload_button=false,
        upload_button_title="Upload",
        download_button=false,
        download_button_title="Download",
        prepare_download=nothing,
        create_directory_button=false,
        create_directory_button_title="New directory",
        delete_button=false,
        delete_button_title="Delete",
        maxpathlength= buttons -> 72 - buttons * 6 ,
        dircontent_rows=5)
    FileBrowser(dir=nothing; settings::FileBrowserSettings)

Server-side file browser widget.

Per default, the browser opens the current working directory.
The widget features a read-only text input field to display the working directory, and a
select element to list the content of that directory. Per default, files and directories
whose name starts with a dot are considered hidden and are not listed.

In the content listing, the first entry is “..”. Double-clicking on this entry makes the
browser change directory to the parent directory. This entry is available as long as the
current directory is not the root directory.
The root directory can be set (argument `root`) so that the browser prevents any access to
the server's filesystem outside the root and below directories.

The content listing is not dynamically updated whenever the working directory's content
changes. The content listing can be manually refreshed with the *Reload* button. This button
is displayed per default; to change this default behavior, pass `reload_button=false`.

The file browser can allow file uploads. Files can be manually uploaded clicking on the
*Upload* button. Per default, this button is not displayed; to make it available, pass
`upload_button=true`.

The regular files can be downloaded clicking on a button visible on hovering the
corresponding raw of the content listing. This opt-in behavior can be enabled with
`download_button=true`.

The main observables are provided by the following functions: [`selectedfile`](@ref),
[`workingdir`](@ref) and [`uploadedfile`](@ref Main.NyxWidgets.FilePickers.uploadedfile).

`prepare_download` is a function that takes the file to be downloaded by the client,
optionally copies it and returns the resulting route.
"""
struct FileBrowser
    model::Model.FileBrowser
    view::View.FileBrowser
    filepicker::Union{Nothing, FilePicker} # widget dependency
end

Model.workingdir(browser::FileBrowser) = workingdir(browser.model)
Model.dircontent(browser::FileBrowser) = dircontent(browser.model)
Model.selectedfile(browser::FileBrowser) = selectedfile(browser.model)
View.selectoption(browser::FileBrowser) = selectoption(browser.view)
View.supports_reload(browser::FileBrowser) = supports_reload(browser.view)
View.supports_upload(browser::FileBrowser) = !isnothing(browser.filepicker)
View.supports_download(browser::FileBrowser) = supports_download(browser.view)
function View.supports_create_directory(browser::FileBrowser)
    supports_create_directory(browser.view)
end
View.supports_delete(browser::FileBrowser) = supports_delete(browser.view)
View.hascontrols(browser::FileBrowser) = hascontrols(browser.view)

function bind(model::Model.FileBrowser, view::View.FileBrowser)
    on(view.selectoption) do entry
        selectfile(model, entry)
    end
    on(model.workingdir) do _
        # the first callback might modify the stored value
        dir = model.workingdir[]
        view.workingdir[] = if dir == model.root
            ROOT
        else
            formatpath(dir, model.root, maxpathlength(view))
        end
    end
    on(dircontent(model)) do entries
        view.dircontent[] = [(isdir ? "directory" : "", entry)
                             for (isdir, entry) in entries]
    end
    supports_reload(view) && on(view.reload) do _
        notify(model.workingdir)
    end
    supports_download(view) && on(view.download) do _
        model.download_request[] = view.hovered_entry[]
    end
    supports_delete(view) && on(view.delete) do _
        model.delete[] = view.hovered_entry[]
    end
end

function bind(model::Model.FileBrowser, view::View.FileBrowser, picker::FilePicker)
    bind(model, view)
    @assert supports_upload(view)
    on(view.upload) do _
        pickfile(picker)
    end
end

bind(model::Model.FileBrowser, view::View.FileBrowser, ::Nothing) = bind(model, view)

bind(browser::FileBrowser) = bind(browser.model, browser.view, browser.filepicker)

Model.selectfile(browser::FileBrowser, dst::AbstractString) = selectfile(browser.model, dst)

function FileBrowser(dir=nothing; root=nothing, settings=nothing, filepicker=nothing,
        prepare_download=nothing, kwargs...)
    model = view = nothing
    if isnothing(settings)
        model = Model.FileBrowser(root; prepare_download=prepare_download)
        view = View.FileBrowser(; kwargs...)
    else
        @assert isnothing(root) && isempty(kwargs) && isnothing(prepare_download)
        model = Model.FileBrowser(settings.root)
        view = View.FileBrowser(settings)
    end
    if supports_upload(view) && isnothing(filepicker)
        filepicker = FilePicker()
    end
    bind(model, view, filepicker)
    selectfile(model, isnothing(dir) ? pwd() : dir)
    FileBrowser(model, view, filepicker)
end

"""
    uploadedfile(filebrowser)

`Dict{String, String}` observable that is notified whenever a file has been uploaded from
the client browser. The keys are *name* and *content*. The corresponding values are the file
name and content (as text) respectively.

A straight-forward implementation of a file writer is:
```julia
on(uploadedfile(filebrowser)) do fileinfo
    parentdir = workingdir(filebrowser)[]
    filepath = joinpath(parentdir, fileinfo["name"])
    open(filepath, "w") do f
        write(f, fileinfo["content"])
    end
    notify(workingdir(filebrowser)) # refresh the directory content
end
```
"""
FilePickers.uploadedfile(browser::FileBrowser) = uploadedfile(browser.filepicker)

Base.isready(browser::FileBrowser) = isready(browser.model)

dom_id(fb::FileBrowser) = dom_id(fb.view)

dom_id!(fb::FileBrowser, val) = dom_id!(fb.view, val)

function Bonito.jsrender(session::Session, browser::FileBrowser)
    isready(browser) || throw("File browser not initialized yet")
    view = browser.view
    for asset in (:js, :css, :theme, :iconset)
        Bonito.jsrender(session, NyxAsset(asset))
    end
    dom = lowerdom(session, view)
    node = dom[:node]
    if register(session, browser.filepicker)
        node = DOM.div(browser.filepicker, node)
        node = Bonito.jsrender(session, node)
    end
    #
    if supports_create_directory(view)
        newdirectory = browser.model.newdirectory
        on(session, Observables.observe(view.createdir)) do _
            evaljs(session, js"""
            let dirname = prompt('New directory name: ', '');
            if (dirname) {
                $newdirectory.notify(dirname);
            }
            """)
        end
    end
    #
    dircontent_dom, selectoption = dom[:dircontent], view.selectoption
    if hascontrols(view)
        relheight = view.dircontent_rows / (view.dircontent_rows - 1)
        onjs(session, view.dircontent, js"""(entries) => {
            let control = $(dom[:entrycontrols]),
                select = $dircontent_dom,
                hovered = $(view.hovered_entry),
                previous;
            NyxWidgets.updateSelectOptions(select, entries, $selectoption);
            let controlTopMax = select.offsetHeight * $relheight;
            Array.from(select.children).forEach(function (option) {
                if (!option.classList.contains('directory')) {
                    option.addEventListener('mouseover', () => {
                        let controlTop = option.offsetTop - select.scrollTop,
                            controlOffsetTop = controlTop - select.offsetTop;
                        if ((0 <= controlOffsetTop) && (controlTop <= controlTopMax)) {
                            if (option.value !== previous) {
                                hovered.notify(option.value);
                                previous = option.value;
                            }
                            control.style.top = `${controlTop}px`;
                            control.style.left = `${select.offsetWidth}px`;
                            control.style.display = 'block';
                        }
                    });
                    option.addEventListener('mouseout', () => {
                        control.style.display = 'none';
                    });
                }
            });
        }""")
    else
        onjs(session, view.dircontent, js"(entries) =>
            NyxWidgets.updateSelectOptions($dircontent_dom, entries, $selectoption)")
    end
    if supports_download(view)
        onjs(session, browser.model.download_ready, js"""(data) => {
            let a = document.createElement('a'),
                path = data[0],
                filename = data[1];
            a.href = path;
            a.download = filename;
            a.click();
            URL.revokeObjectURL(a.href);
        }""")
    end
    notify(dircontent(browser))
    workingdir_dom, workingdir = dom[:workingdir], view.workingdir
    Bonito.onload(session, workingdir_dom, js"""(textinput) => {
        textinput.value = $(workingdir[]);
    }""")
    return node
end

function standalone(root=nothing; title="File browser", port=9284, tailwind=false)
    app = App(; title=title) do session
        kwargs = Dict{Symbol, Any}(:root=>root, :upload_button=>true)
        if tailwind
            kwargs[:maxpathlength] = buttons -> 58 - buttons * 6
        end
        filebrowser = FileBrowser(; kwargs...)
        on(selectedfile(filebrowser)) do file
            @info "Selecting file" file
        end
        on(workingdir(filebrowser)) do dir
            @info "Changing directory" dir
        end
        on(uploadedfile(filebrowser)) do fileinfo
            filename = fileinfo["name"]
            content = fileinfo["content"]
            @info "Getting file" filename content
        end
        tailwind && Bonito.jsrender(session, Bonito.TailwindCSS)
        Bonito.jsrender(session, filebrowser)
    end
    server = Server(app, "127.0.0.1", port)
    Bonito.HTTPServer.openurl("http://127.0.0.1:$port")
    return server
end

end
