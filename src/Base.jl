module Base

using Bonito
using Observables

export NyxAsset, NyxJS, NyxCSS, NyxTheme, NyxIconSet, Cache, register, Node, lowerdom,
       dom_id, dom_id!, dom_selector, Icon, Button, ToggleButton, Label, Div

"""
    const NyxJS = Bonito.Asset(...)

NyxWidgets javascript extension.

If required, render your Bonito app with:

```julia
import NyxWidgets: NyxJS

App() do session
    # ...
    Bonito.jsrender(session, NyxJS)
    # ...
end
```
"""
const NyxJS = Bonito.Asset(joinpath(@__DIR__, "nyxwidgets.js"))
const NyxCSS = Bonito.Asset(joinpath(@__DIR__, "nyxwidgets.css"))
const BonitoTheme = Bonito.Asset(joinpath(@__DIR__, "nyx-bonito-theme.css"))
const MaterialIconSet = Bonito.Asset(joinpath(@__DIR__, "nyx-material-icons.css"))

const theme = Ref{Union{Nothing, Bonito.Asset}}(BonitoTheme)
const iconset = Ref{Union{Nothing, Bonito.Asset}}(MaterialIconSet)

struct NyxAsset
    type
end

const NyxTheme = NyxAsset(:theme)
const NyxIconSet = NyxAsset(:iconset)

function Bonito.jsrender(session::Session, asset::NyxAsset)
    asset = if asset.type === :theme
        theme[]
    elseif asset.type === :iconset
        iconset[]
    elseif asset.type === :js
        NyxJS
    elseif asset.type === :css
        NyxCSS
    end
    isnothing(asset) || Bonito.jsrender(session, asset)
    return nothing
end

## complementary deduplication

struct Cache{S, T}
    lock::ReentrantLock
    cache::AbstractDict{S, T}
end

Cache{S, T}() where {S, T} = Cache{S, T}(ReentrantLock(), Dict{S, T}())

Base.lock(f::Function, cache::Cache) = lock(f, cache.lock)

# getindex and setindex! are the only methods that take the lock; others are expected to be
# called in a lock block
function Base.getindex(cache::Cache{S, T}, session::S) where {S, T}
    lock(cache.lock) do
        get!(T, cache.cache, session)
    end
end

function Base.setindex!(cache::Cache{S, T}, val::T, session::S) where {S, T}
    lock(cache.lock) do
        cache.cache[session] = val
    end
end

Base.haskey(cache::Cache{S}, session::S) where {S} = haskey(cache.cache, session)

Base.keys(cache::Cache) = keys(cache.cache)
Base.values(cache::Cache) = values(cache.cache)
Base.pairs(cache::Cache) = pairs(cache.cache)

Base.isempty(cache::Cache) = isempty(cache.cache)
Base.length(cache::Cache) = length(cache.cache)

const WidgetCache = Cache{String, Set{UInt64}}

const __widget_cache__ = WidgetCache()

"""
    register(session, widget)

Caches widget `widget` and returns `true` if the widget is registered for the first time or
`false` if it has already been registered.

`register` is meant to conditionally insert elements in the DOM. The original use case is
multiple [`FileBrowser`](@ref Main.NyxWidgets.FileBrowsers.FileBrowser] depending on a
common [`FilePicker`](@ref Main.NyxWidgets.FilePickers.FilePicker].
"""
register(session::Session, widget) = register(session.id, widget)

function register(session::String, widget)
    if isnothing(widget)
        return false
    else
        lock(__widget_cache__) do
            ids = __widget_cache__[session]
            id = objectid(widget)
            if id in ids
                return false
            else
                push!(ids, id)
                return true
            end
        end
    end
end

## DOM nodes

const Node = Bonito.Hyperscript.Node{Bonito.Hyperscript.HTMLSVG}

dom_id(widget) = widget.dom_id[]

dom_id() = Ref("")

dom_id(node::Node) = Bonito.uuid(nothing, node)

dom_id(session::Session) = Bonito.root_session(session).dom_uuid_counter

dom_id!(widget, id::AbstractString) = (widget.dom_id[] = id)

dom_id!(widget, node::Node) = dom_id!(widget, Bonito.uuid(nothing, node))

dom_id!(widget) = dom_id!(widget, "")

"""
    dom_selector(widget)

DOM query selector for Javascript function `document.querySelector` to locate a widget in
the DOM after rendering, *i.e.* after calling `Bonito.jsrender` or `NyxWidgets.lowerdom`.

This can be useful for accessing the DOM representation of widgets nested in a widget.
Let us consider an icon nested in a button:
```julia
icon = Icon(iconclass)
button = Button(icon, observable; class=buttonclass)
node = Bonito.jsrender(session, button)
```
Bonito offers easy access to the button in Javascript, for example with:
```julia
Bonito.evaljs(session, js\"\"\"
var button = \$node;
\"\"\")
```
`\$node` is interpolated as a `document.querySelector(...)` expression.

`dom_selector` allows similarly accessing the nested `icon` widget:
```
selector = dom_selector(icon)
Bonito.evaljs(session, js\"\"\"
var icon = document.querySelector(\$selector);
\"\"\")
```
"""
function dom_selector(widget)
    id = dom_id(widget)
    isempty(id) && error("no DOM ID available; the widget has not been rendered yet")
    return dom_selector(id)
end
dom_selector(id::Union{Integer, AbstractString}) = "[data-jscall-id=\"$id\"]"

"""
    lowerdom(widget)
    lowerdom(session, widget)

Lower a view to a `Bonito.Hyperscript.Node` representation.

NyxWidgets views implement `lowerdom`, which in some cases is enough to implement
`Bonito.jsrender`.

`jsrender` takes a DOM and mutates the nodes to assign IDs. Subsequent calls to `jsrender`
on the same DOM do not assign IDs again. As a consequence, a `Node` can be rendered only
once and should be recreated for example on every page reload.

In contrast, NyxWidgets views are immutable and can be rendered multiple times, for example
in multiple HTTP sessions (aka multiple `Bonito.Session`s) part of a single user session.

To do so, NyxWidgets views do not store `Node`s; they instead generate the `Node`-based
representation on every call to `jsrender`. This lowering mechanism is separated from
`jsrender` using `lowerdom(widget)` (no `Session` involved).

`lowerdom(session, widget)` is typically called inside `jsrender` as a more powerful variant
of `jsrender(session, lowerdom(widget))`.

`lowerdom` has two return types. For most widgets, it returns a `Node`. For others, it
returns a `Dict{Symbol, Node}` that references the top node with key `:node` plus other
children nodes of interest, typically to register listeners in `jsrender`.
"""
function lowerdom(session::Session, widget)
    lowered = lowerdom(widget)
    node = lowered isa Dict ? lowered[:node] : lowered
    node = Bonito.jsrender(session, node)
    dom_id!(widget, node)
    if lowered isa Dict
        lowered[:node] = node
        lowered
    else
        node
    end
end

lowerdom(node::Node) = node

## icons and buttons

struct Icon
    class
    dom_id
end

Icon(class) = Icon(class, dom_id())

lowerdom(icon::Icon) = DOM.div(; class=icon.class)

Bonito.jsrender(session::Session, icon::Icon) = lowerdom(session, icon)

icon(class) = DOM.div(; class=class)

"""
    Button(child, target; class=nothing, title=nothing)

Basic button that triggers observable `target`.

See also [`ToggleButton`](@ref).
"""
struct Button <: AbstractObservable{Bool}
    child
    target::AbstractObservable{Bool}
    onclick
    title
    attributes
    dom_id
end

function Button(child, target::AbstractObservable{Bool}=Observable(false);
        title=nothing, kwargs...)
    onclick = if isnothing(title)
        js"() => { $target.notify(true); }"
    else
        title_main = split(title, "\n"; limit=2)[1]
        js"""() => {
            console.log("click on button $title_main");
            $target.notify(true);
        }"""
    end
    attributes = Dict{Symbol, Any}(kwargs)
    Button(child, target, onclick, title, attributes, dom_id())
end

function lowerdom(button::Button)
    DOM.button(button.child; onclick=button.onclick, title=button.title,
               button.attributes...)
end

Bonito.jsrender(session::Session, button::Button) = lowerdom(session, button)

Observables.observe(button::Button) = button.target

"""
    ToggleButton(child, target, toggle=nothing;
                 class=nothing, title=nothing, activeclass="active")

Button that toggles the state of observable `target`.

An intermediate observable is created to receive the click events on the button. This
observable can be set externally and passed as argument `toggle`.

When `target` stores a `true`, the button element in the DOM gets an additional class
`activeclass`. This allows styling the button depending on its state.

See also [`Button`](@ref).
"""
struct ToggleButton <: AbstractObservable{Bool}
    button::Button
    target::AbstractObservable{Bool}
    activeclass::AbstractString
end

function ToggleButton(child, target,
        toggle::Union{Nothing, <:AbstractObservable{Bool}}=nothing;
        activeclass="active", kwargs...)
    if isnothing(toggle)
        toggle = Observable(target[])
        on(toggle) do _
            target[] = !target[]
        end
    end
    button = Button(child, toggle; kwargs...)
    ToggleButton(button, target, activeclass)
end

dom_id(toggle::ToggleButton) = dom_id(toggle.button)

dom_id!(toggle::ToggleButton, val) = dom_id!(toggle.button, val)

lowerdom(toggle::ToggleButton) = lowerdom(toggle.button)

function Bonito.jsrender(session::Session, toggle::ToggleButton)
    button = Bonito.jsrender(session, toggle.button)
    activeclass = toggle.activeclass
    updateclass = js"""(b) => {
        if (b) {
            $button.classList.add($activeclass);
        } else {
            $button.classList.remove($activeclass);
        }
    }"""
    Bonito.onjs(session, toggle.target, updateclass)
    # TODO: use onload instead of notifying the toggle.target observable
    toggle.target[] && notify(toggle.target)
    # TODO: should we call jsrender on toggle.child for genericity?
    return button
end

Observables.observe(toggle::ToggleButton) = toggle.target

Base.setindex!(toggle::ToggleButton, b::Bool) = b && notify(toggle.button)

struct Label
    label
    attributes
    dom_id
end

Label(label; kwargs...) = Label(label, Dict{Symbol, Any}(kwargs), dom_id())

lowerdom(label::Label) = DOM.label(label.label; label.attributes...)

Bonito.jsrender(session::Session, label::Label) = lowerdom(session, label)

struct Div
    children
    attributes
    dom_id
end

function Div(children::Vector; style=nothing, kwargs...)
    attributes = Dict{Symbol, Any}(kwargs)
    if style === :row
        attributes[:style] = "display: flex; flex-direction: row;"
    elseif style === :col
        attributes[:style] = "display: flex; flex-direction: col;"
    elseif !isnothing(style)
        attributes[:style] = style
    end
    Div(children, attributes, dom_id())
end

Div(children...; kwargs...) = Div(collect(children); kwargs...)

lowerdom(div::Div) = DOM.div(div.children; div.attributes...)

Bonito.jsrender(session::Session, div::Div) = lowerdom(session, div)

end
