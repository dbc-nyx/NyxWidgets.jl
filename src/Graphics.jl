module Graphics

using Observables
import Bonito: Bonito, DOM, Session, @js_str
import ..Base: lowerdom
using Format
using Colors

"""
    SVG(children; hoverable=false, attributes...)

Top abstraction for a SVG document.
"""
struct SVG
    children::AbstractVector
    hoverable::Bool
    attributes
end

function SVG(children; hoverable=false, attributes...)
    SVG(children, hoverable, Dict{Symbol, Any}(attributes))
end

struct Title
    title
    attributes
end

Title(title; attributes...) = Title(title, Dict{Symbol, Any}(attributes))

struct Text
    text
    attributes
end

Text(text; attributes...) = Text(text, Dict{Symbol, Any}(attributes))

"""
    Polygon(points; style=Dict{String, Any}())

Abstraction for a SVG *polygon* element with dynamic shape and style.
"""
struct Polygon
    points::AbstractObservable
    style::AbstractObservable{<:AbstractDict}
    svgpoints::Observable{String}
    svgstyle::Observable{String}
end

function Polygon(points; style=Dict{String, Any}())
    points, style = observe(points), observe(style)
    Polygon(points, style, map(svgpoints, points), map(svgstyle, style))
end

observe(obs::AbstractObservable) = obs
observe(val) = Observable(val)

"""
    G(children; attributes...)

Abstraction for a SVG *g* element with event handling.

Front-to-back events are specified with [`event`](@ref) which should be called before
the *g* element is rendered.

Back-to-front events are specified with [`class`](@ref) which should also be called before
the *g* element is rendered.
"""
struct G
    children::AbstractVector
    events::Dict{String, Observable{Bool}}
    classes::Dict{String, Observable{Bool}}
    hoverable::Bool
    attributes
end

G(children; attributes...) = G(
    children,
    Dict{String, Observable{Bool}}(),
    Dict{String, Observable{Bool}}(),
    false,
    Dict{Symbol, Any}(attributes),
)

children(elem) = elem.children

"""
    event(elem, jsevent)

Returns an `Observable` that is notified on `jsevent` which a javascript event as supported
by `addEventListener`.

See for example [`click`](@ref).
"""
event(elem, e) = get!(elem.events, e, Observable(false))

"""
    click(elem)

Returns an `Observable` that is notified on a click on element `elem`.
"""
click(elem) = event(elem, "click")

"""
    class(elem, cls)

Returns an `Observable` that can be set to true or false, respectively to add or remove a
CSS class to element `elem`.

See for example [`active`](@ref).
"""
class(elem, c) = get!(elem.classes, c, Observable(false))

"""
    active(elem)

Returns an `Observable` that can be set to true or false, respectively to add or remove the
*active* class to element `elem`.
"""
active(elem) = class(elem, "active")
hidden(elem) = class(elem, "hidden")

function class(elem::G)
    classes = String[]
    if haskey(elem.attributes, :class)
        push!(classes, elem.attributes[:class])
    end
    foreach(pairs(elem.classes)) do c
        c.second[] && push!(classes, c.first)
    end
    return join(classes, " ")
end

attributes(elem) = elem.attributes

function attributes(g::G)
    attributes = g.attributes
    class′= class(g)
    if !isempty(class′)
        attributes = Dict(attributes)
        attributes[:class] = class′
    end
    attributes
end

function svgpoints(pts::Vector{Tuple{F, F}}; digits=2) where {F<:AbstractFloat}
    fmt = FormatExpr("{:.$(digits)f},{:.$(digits)f}")
    join([format(fmt, pt[1], pt[2]) for pt in pts], " ")
end

function svgpoints(pts::Vector{Tuple{I, I}}) where {I<:Integer}
    join(["$x,$y" for (x, y) in pts], " ")
end

function svgpoints(pts::Vector{<:AbstractVector})
    svgpoints([tuple(pt...) for pt in pts])
end

function svgstyle(sty)
    join(["$k: $v;" for (k, v) in pairs(sty)])
end

svgstyle(sty::AbstractString) = string(sty)

## rendering

function register_events(session, elem, node)
    foreach(pairs(elem.events)) do e
        evt, obs = e
        Bonito.onload(session, node, js"""(node) => {
            node.addEventListener($evt, () => { $obs.notify(true); });
        }""")
    end
end

function register_classes(session, elem, node)
    foreach(pairs(elem.classes)) do c
        cls, obs = c
        Bonito.onjs(session, obs, js"""(b) => {
            b ? $node.classList.add($cls) : $node.classList.remove($cls)
        }""")
    end
end

function register_hooks(session, elem, node)
    if hasproperty(elem, :hoverable) && elem.hoverable
        Bonito.onload(session, node, js"""(node) => {
        var children = Array.from(node.children);
        var active;
        children.forEach(child => {
            child.addEventListener('mouseenter', () => {
                if (active !== undefined) { active.classList.remove('hovered'); }
                node.appendChild(child);
                child.classList.add('hovered');
                active = child;
            });
            child.addEventListener('mouseout', () => {
                if (active === child) {
                    child.classList.remove('hovered');
                    delete window.active;
                }
            });
        });
        }""")
    end
end

lowerdom(title::Title) = Bonito.SVG.title(title.title; title.attributes...)

Bonito.jsrender(session::Session, title::Title) = Bonito.jsrender(session, lowerdom(title))

lowerdom(text::Text) = Bonito.SVG.text(text.text; text.attributes...)

Bonito.jsrender(session::Session, text::Text) = Bonito.jsrender(session, lowerdom(text))

function lowerdom(polygon::Polygon)
    Bonito.SVG.polygon(; points=polygon.svgpoints[], style=polygon.svgstyle[])
end

function Bonito.jsrender(session::Session, polygon::Polygon)
    node = Bonito.jsrender(session, lowerdom(polygon))
    Bonito.onjs(session, polygon.svgpoints, js"points => { $node.points = points; }")
    Bonito.onjs(session, polygon.svgstyle, js"style => { $node.style = style; }")
    return node
end

lowerdom(g::G) = Bonito.SVG.g(children(g); attributes(g)...)

function Bonito.jsrender(session::Session, g::G)
    node = Bonito.jsrender(session, lowerdom(g))
    register_classes(session, g, node)
    register_events(session, g, node)
    register_hooks(session, g, node)
    return node
end

lowerdom(graphics::SVG) = DOM.svg(children(graphics); attributes(graphics)...)

function Bonito.jsrender(session::Session, graphics::SVG)
    node = Bonito.jsrender(session, lowerdom(graphics))
    register_hooks(session, graphics, node)
    return node
end

# interaction

function opacity(p::Polygon)
    obs = Observable(0.1)
    on(obs) do opacity
        p.style[]["fill-opacity"] = opacity
        notify(p.style)
    end
    return obs
end

function color(p::Polygon; colorscheme)
    obs = Observable(0.0)
    on(obs) do color
        p.style[]["fill"] = "#" * hex(get(colorscheme, color))
        notify(p.style)
    end
    return obs
end

end
