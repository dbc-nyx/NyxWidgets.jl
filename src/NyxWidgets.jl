module NyxWidgets

include("Base.jl")
include("FilePickers.jl")
include("FileBrowsers.jl")
include("Graphics.jl")
include("Muscles.jl")
include("Players.jl")
include("AnimatedLayers.jl")

import .Base: NyxAsset, NyxJS, NyxCSS, NyxTheme, NyxIconSet, theme, iconset, Node, lowerdom,
              dom_id, dom_id!, dom_selector, Icon, Button, ToggleButton, Label, Div

end
