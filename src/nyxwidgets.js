const NyxWidgets = (function () {

  function updateSelectOptions(selectElement, selectOptions, jlObserver) {
    while (selectElement.options.length > 0)
    selectElement.remove(0);
    for (let i = 0; i < selectOptions.length; i++) {
      const option = document.createElement('option');
      if (Array.isArray(selectOptions[i])) {
        const [cls, val] = selectOptions[i];
        option.value = option.text = val;
        if (cls) option.classList.add(cls);
      } else {
        option.value = option.text = selectOptions[i];
      }
      if (jlObserver !== undefined) {
        option.ondblclick = () => { jlObserver.notify(option.value) };
      }
      selectElement.append(option);
    }
  }

  function focusOnTimeSlider() {
    var context = document;
    var clsname = 'nyx-player';
    var i = 0;
    if (i < arguments.length && typeof arguments[i] !== 'string') {
      context = arguments[i];
      i += 1;
    }
    if (i < arguments.length) {
      clsname = arguments[i];
      i += 1;
    }
    context.querySelector(`div.${clsname} > input[type=range]`).focus()
  }

  return {
    updateSelectOptions,
    focusOnTimeSlider
  };
})();
