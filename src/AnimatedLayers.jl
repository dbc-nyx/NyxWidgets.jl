module AnimatedLayers

import ..Base: NyxJS, Button, Icon, Node, lowerdom, dom_selector, dom_id
using ..Players
using Bonito

export LayeredAnimation, activelayer

"""
    LayeredAnimation(times, layers...; playerstyle=nothing, layerstyle=nothing,
                     playbackspeeds=nothing)
    LayeredAnimation(playerview, layers...; layerstyle=nothing)

A `LayeredAnimation` features a [`PlayerView`](@ref Main.NyxWidgets.Players.PlayerView) and
a stack of overlaid layers that can be any DOM element or widget.

At any time, a single layer is visible (the top one in the stack), and the stack can be
manipulated with [`open`](@ref) and [`close`](@ref).

`LayeredAnimation` is an
[`AbstractPlayer`](@ref Main.NyxWidgets.Players.Model.AbstractPlayer) and can consequently
be observed. At the moment, the different layers cannot admit individual listeners.
Listeners are expected to get the active layer with [`activelayer`](@ref), and switch logic
depending on the obtained layer.

```julia
on(animation) do timestep
    layer = activelayer(animation)
    # if `layer` is first one
    #    evaluate first layer callback with argument `timestep`
    # elseif `layer` is second one
    #    evaluate second layer callback with argument `timestep`
    # etc
end
```

"""
struct LayeredAnimation <: AbstractPlayer
    player::PlayerView
    layers::Vector
    stack::Vector{Int}
    layerstyle::Union{Nothing, AbstractString}
    bind::Bool
    switchlayer
    closelayer
    closebutton
    dom_id
end

function LayeredAnimation(player::PlayerView, layers...; layerstyle=nothing, bind=true,
        kwargs...)
    @assert !isempty(layers)
    layers = collect(layers)
    stack = Int[]
    switchlayer = Observable{Union{Nothing, Tuple{Int, Int, String}}}(nothing)
    closelayer = Observable(false)
    closebutton = Button(Icon("nyx-close-icon"), closelayer; class="nyx-icon-button",
                         title="Close")
    anim = LayeredAnimation(player, layers, stack, layerstyle, bind, switchlayer,
                            closelayer, closebutton, dom_id())
    on(closelayer) do _
        close(anim)
    end
    return anim
end

function LayeredAnimation(times::AbstractVector{<:Real}, layers...; playbackspeeds=nothing,
        playerstyle=nothing, kwargs...)
    viewargs = Dict{Symbol, Any}()
    if !isnothing(playerstyle)
        viewargs[:style] = playerstyle
    end
    player = Player(times)
    playerview = PlayerView(player, playbackspeeds; viewargs...)
    LayeredAnimation(playerview, layers...; kwargs...)
end

player(anim) = anim.player.player

Players.isplaying(anim::LayeredAnimation) = isplaying(player(anim))

Players.timestep(anim::LayeredAnimation) = timestep(player(anim))

Players.timestamps(anim::LayeredAnimation) = timestamps(player(anim))

Players.speed(anim::LayeredAnimation) = Players.speed(player(anim))

Players.repeat(anim::LayeredAnimation) = Players.repeat(player(anim))

Players.task(anim::LayeredAnimation) = Players.task(player(anim))

"""
    activelayer(animation)
    activelayer(Int, animation)

Returns the currently active layer, *i.e.* the layer at the top of the layer stack.

If type `Int` is passed as first argument, the active layer is returned as an index.
"""
activelayer(::Type{Int}, anim) = isempty(anim.stack) ? 1 : anim.stack[end]

activelayer(anim) = anim.layers[activelayer(Int, anim)]

"""
    Base.close(animation::LayeredAnimation)

Pops the top layer and reveal the layer below.

Throws an `ErrorException` if the top layer is the bottom one, which cannot be closed.

See also [`open`](@ref).
"""
function Base.close(anim::LayeredAnimation)
    isempty(anim.stack) && error("cannot close main layer")
    prev = pop!(anim.stack)
    switch(anim, prev)
    return anim.layers[prev]
end

function switch(anim, previous)
    notify(anim)
    current = activelayer(Int, anim)
    buttonvisibility = current == 1 ? "hidden" : "visible"
    anim.switchlayer[] = (current, previous, buttonvisibility)
end

"""
    Base.open(animation::LayeredAnimation, layer)

Appends a layer on top of the stack of opened layers.

Layer `layer` must be found in the list of layers specified at initialization time.
A layer cannot be opened twice.

An `ErrorException` is thrown otherwise.

See also [`close`](@ref).
"""
function Base.open(anim::LayeredAnimation, layer)
    k = findfirst(l->l===layer, anim.layers)
    isnothing(k) && error("unknown layer")
    open(anim, k)
end

function Base.open(anim::LayeredAnimation, layer::Int)
    (1 <= layer <= length(anim.layers)) || throw(KeyError(layer))
    layer == 1 && error("main layer already opened")
    layer in anim.stack && error("layer already opened")
    prev = activelayer(Int, anim)
    push!(anim.stack, layer)
    switch(anim, prev)
    return anim.layers[layer]
end

function lowerdom(anim::LayeredAnimation)
    attrs = Dict(:class => "nyx-animated-layers")
    if !(isnothing(anim.layerstyle) || isempty(anim.layerstyle))
        attrs[:style] = anim.layerstyle
    end
    layers = DOM.div(anim.layers; attrs...)
    layercontrols = DOM.div(anim.closebutton; class="nyx-layer-controls")
    return DOM.div(layercontrols, layers, anim.player; class="nyx-layered-animation")
end

function Bonito.jsrender(session::Session, anim::LayeredAnimation)
    Bonito.jsrender(session, NyxJS)
    node = lowerdom(session, anim)
    anim.bind && Players.bind(session, node)
    selectors = dom_selector.(anim.layers)
    closebutton = dom_selector(anim.closebutton)
    on(session, anim.switchlayer) do args
        current, previous, buttonvisibility = args
        current = selectors[current]
        previous = selectors[previous]
        evaljs(session, js"""
        var closebutton = document.querySelector($closebutton);
        var previous = document.querySelector($previous);
        var current = document.querySelector($current);
        previous.style.visibility = 'hidden';
        previous.classList.remove('active');
        current.classList.add('active');
        current.style.visibility = 'visible';
        closebutton.style.visibility = $buttonvisibility;
        NyxWidgets.focusOnTimeSlider($node);
        """)
    end
    if !isempty(anim.stack)
        anim.switchlayer[] = (activelayer(Int, anim), 1, "visible")
    end
    return node
end

end
