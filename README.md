# NyxWidgets

[![Documentation](https://img.shields.io/badge/docs-main-9cf.svg)](http://dbc-nyx.gitlab.io/NyxWidgets.jl/)

Basic widgets built on top of [Bonito](https://github.com/SimonDanisch/Bonito.jl):

* `FileBrowser` allows browsing files server-side, with optional file upload capability;
* `Player` is a time controller with a play/pause button, step forward and step backward buttons, a repeat button, a time slider and time label; optionally a playback speed selector;
* `LayeredAnimations` is a container for multiple overlaid divs, with a single visible layer at a time, a close button on the top corner for layers other than the first one, and a player at the bottom; the player updates the visible layer only;
* `MuscleWidget` draws SVG polygons to represent the *Drosophila* larval muscles in one abdominal segment.

The above widgets are showcased in test examples in the *test* directory.

## Installation

### Julia

NyxWidgets is primarily a Julia package. It is not registered in the General registry, hence, in a Julia project:
```
julia --project -e 'using Pkg; Pkg.add(url="https://gitlab.com/dbc-nyx/NyxWidgets.jl")'
```

### Python

The [test/muscles/app.py MuscleWidget example app](test/muscles/app.py) includes a setup procedure for a mixed Julia/Python project, but also give pointers for installing MuscleWidget.jl in a Python-only project.

In the latter case, you can simply install the juliacall Python package:
```
pip install juliacall
```
and run a bootstrap procedure once, in Python:
```
import juliacall
jl = juliacall.Main
try:
    jl.seval('using Bonito;')
except juliacall.JuliaError:
    jl.seval('''
    using Pkg
    Pkg.add(url="https://gitlab.com/dbc-nyx/NyxWidgets.jl")
    Pkg.add("Bonito")
    ''')
```
More Julia dependencies can be installed, and version constraints can also be specified. Learn more from the [Pkg documentation](https://docs.julialang.org/en/v1/stdlib/Pkg/).
