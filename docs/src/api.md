
## FileBrowsers

```@autodocs
Modules = [NyxWidgets.FileBrowsers, NyxWidgets.FileBrowsers.Model, NyxWidgets.FileBrowsers.View]
```

## Players

```@autodocs
Modules = [NyxWidgets.Players.Model, NyxWidgets.Players]
```

## Muscles widgets

```@autodocs
Modules = [NyxWidgets.Muscles]
```

## Animated layers

```@autodocs
Modules = [NyxWidgets.AnimatedLayers]
```

## Support logic

### Base

```@autodocs
Modules = [NyxWidgets.Base]
```

### FilePickers

```@autodocs
Modules = [NyxWidgets.FilePickers]
```

### SVG Graphics

```@autodocs
Modules = [NyxWidgets.Graphics]
```
