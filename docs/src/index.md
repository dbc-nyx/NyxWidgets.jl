# NyxWidgets.jl documentation

At present, this documentation is limited to the API.

A general introduction and installation procedure is described on the [project's home](https://gitlab.com/dbc-nyx/NyxWidgets.jl#nyxwidgets).
