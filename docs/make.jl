#using Pkg
#Pkg.develop(path="..")

using Documenter, NyxWidgets
using Documenter.Remotes: GitLab

makedocs(;
    modules=[NyxWidgets],
    sitename="NyxWidgets.jl",
    pages=[
        "Home" => "index.md",
        "API" => "api.md",
    ],
    repo = GitLab("dbc-nyx", "NyxWidgets.jl"),
)

deploydocs(
    repo = "gitlab.com/dbc-nyx/NyxWidgets.jl.git",
)
