#!/bin/bash
#=
PROJECT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

if [ -z "$JULIA" ]; then JULIA=julia; fi

exec "$JULIA" --project="$PROJECT_DIR" --startup-file=no --color=yes\
    -iq "${BASH_SOURCE[0]}" $@
=#

using Bonito
using NyxWidgets.FileBrowsers

# modified copy of FileBrowsers.standalone
function standalone(root=nothing; title="File browser", port=9284, tailwind=false)
    # the code below is found outside the app block to test the integrity of the view when
    # rendered multiple times, with page reloads
    kwargs = Dict{Symbol, Any}(:root=>root, :upload_button=>true, :download_button=>true,
                               :create_directory_button=>true, :delete_button=>true)
    if tailwind
        kwargs[:maxpathlength] = buttons -> 58 - buttons * 6
    end
    filebrowser = FileBrowser(; kwargs...)
    #
    on(selectedfile(filebrowser)) do file
        @info "Selecting file" file
    end
    wd = workingdir(filebrowser)
    on(wd) do dir
        @info "Changing directory" dir
    end
    on(uploadedfile(filebrowser)) do fileinfo
        filepath = joinpath(wd[], fileinfo["name"])
        open(filepath, "w") do f
            write(f, fileinfo["content"])
        end
        notify(wd)
    end

    app = App(; title=title) do session
        #
        tailwind && Bonito.jsrender(session, Bonito.TailwindCSS)
        Bonito.jsrender(session, filebrowser)
    end
    mkpath("tmp")
    server = Server(app, "127.0.0.1", port)
    Bonito.HTTPServer.openurl("http://127.0.0.1:$port")
    return server
end

if abspath(PROGRAM_FILE) == @__FILE__
    standalone(homedir())
end
