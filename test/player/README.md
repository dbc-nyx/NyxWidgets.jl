# Player example app

To install and run a standalone player:

```
make
./app.jl
```

The 3 rectangles are included to test the autofocus feature on the time slider, so that the arrow keys can be used in more circumstances to step forward or backward in time.
Currently, the slider is autofocused on the mouse pointer entering the player or rectangles. A mouse click shifts the focus away.
