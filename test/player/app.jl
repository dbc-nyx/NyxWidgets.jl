#!/bin/bash
#=
PROJECT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

if [ -z "$JULIA" ]; then JULIA=julia; fi
PYTHON="$PROJECT_DIR/bin/python"

PYTHON="$PYTHON" exec "$JULIA" --project="$PROJECT_DIR" --startup-file=no --color=yes\
    -iq "${BASH_SOURCE[0]}" $@
=#

using Bonito
using NyxWidgets.Players
import NyxWidgets.Base: NyxJS

function main(; title="Player", port=9284)
    # outside of App to test integrity after reload
    controller = Player(0.04:0.04:4)
    speeds = ["0.25", "0.5", "0.75", "1.0", "1.25", "1.5", "2.0"]
    view = PlayerView(controller, speeds)

    app = App(; title=title) do session
        dom = view
        style(color) = "width:20rem;height:40rem;border:solid 1px $color;margin:1rem 0 1rem 1rem;"
        area1 = DOM.div(; style=style("red"),
                        onmouseenter=js"() => NyxWidgets.focusOnTimeSlider()")
        area2 = DOM.div(; style=style("green"),
                        onmouseenter=js"() => NyxWidgets.focusOnTimeSlider('nyx-player')")
        area3 = DOM.div(; style=style("blue"))
        areas = DOM.div(area1, area2, area3; style="display:flex;flex-direction:row;")
        dom = DOM.div(areas, view; style="width: 50%;")
        Players.bind(session, dom)
        #Bonito.jsrender(session, Bonito.TailwindCSS)
        Bonito.jsrender(session, NyxJS)
        Bonito.jsrender(session, dom)
    end
    server = Server(app, "0.0.0.0", port)
    Bonito.HTTPServer.openurl("http://127.0.0.1:$port")
    return server
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
