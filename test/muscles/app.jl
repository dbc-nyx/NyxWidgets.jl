#!/bin/bash
#=
PROJECT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

if [ -z "$JULIA" ]; then JULIA=julia; fi
PYTHON="$PROJECT_DIR/bin/python"

PYTHON="$PYTHON" exec "$JULIA" --project="$PROJECT_DIR" --startup-file=no --color=yes\
    -iq "${BASH_SOURCE[0]}" $@
=#

using Bonito
import NyxWidgets.Base: NyxAsset, ToggleButton, Icon
using NyxWidgets.Muscles

include("import.jl")
include("roll.jl")


# for access from the REPL
const __widget__ = Ref{Union{Nothing, MuscleWidget, MultiSegmentMuscleWidget}}(nothing)

# player
const __step__ = Ref(0)
const __play__ = Observable(false)

# activitation sequence
const muscle_activation_sequence = Ref(roll_muscle_activation_sequence)
const stepmax = Ref(39)


function mainloop(model, timestep)
    step = __step__[]
    while __play__[]
        t = time()
        drawframe(model, step)
        __step__[] = step = step + 1
        if step > stepmax[]
            __step__[] = 0
            __play__[] = false
            break
        else
            dt = timestep - (time() - t)
            0.001 <= dt && sleep(dt)
        end
    end
end

function drawframe(model, step)
    @info "Drawing frame" step
    sequence = muscle_activation_sequence[]
    for (muscle, mesh) in muscles(model)
        muscle = muscle.name
        Graphics.hidden(mesh)[] && continue
        style = Muscles.polygon(mesh).style
        if haskey(sequence, muscle) && haskey(sequence[muscle], step)
            color = sequence[muscle][step]
            style[]["fill"] = color
            style[]["fill-opacity"] = 1
            notify(style)
        elseif !isempty(style[])
            empty!(style[])
            notify(style)
        end
    end
end

function main(jsonfile="muscles.json"; title="Meshes", port=9284, fps=4)
    # To regenerate the JSON file, uncomment the below line and pass `jsonfile` as first
    # argument to the widget's constructor (`MuscleWidget` or `MultiSegmentMuscleWidget`).
    #isfile(jsonfile) || importmeshes(jsonfile)

    # TODO: test each of the following use cases
    muscles = MuscleWidget(; style="width:20%;", musclelabel=true, segmentselector=true,
                           segmentlabel=true, segment="A1")
    #muscles = MultiSegmentMuscleWidget(; musclelabel=false, style="width:50%;")
    #muscles = MultiSegmentMuscleWidget(; musclelabel=true, style="width:50%;")

    __widget__[] = muscles
    # listeners
    on(Muscles.click(muscles)) do (muscle, mesh)
        @info "Click on muscle" muscle
        obs = Graphics.active(mesh)
        obs[] = !obs[]
    end
    # animation
    viewplay = Observable(false)
    on(viewplay) do _
        __play__[] = !__play__[]
    end
    on(__play__) do b
        @info (b ? "Playing" : "Pause")
        if b
            Threads.@spawn begin
                mainloop(muscles, 1.0 / fps)
            end
        end
    end
    player = ToggleButton(Icon("nyx-play-pause-icon"), viewplay;
                          class="nyx-icon-button")

    app = App(; title=title) do session
        # HTML document
        doc = DOM.div(muscles, player)
        #Bonito.jsrender(session, Bonito.TailwindCSS)
        Bonito.jsrender(session, NyxAsset(:iconset))
        Bonito.jsrender(session, doc)
    end
    # serve the app
    server = Server(app, "127.0.0.1", port)
    Bonito.HTTPServer.openurl("http://127.0.0.1:$port")
    # show notice
    println("\ntry:\nusing ColorSchemes\nopacity = Graphics.opacity(__widget__[], \"VO4\")\ncolor = Graphics.color(__widget__[], \"VO4\"; colorscheme=ColorSchemes.leonardo)\nopacity[] = 1\ncolor[] = 1\n")
    return server
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end

