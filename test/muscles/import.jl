
using PyCall
using Random
using LazySets
using StatsBase
using LinearAlgebra
using NyxWidgets.Graphics

np = pyimport("numpy")

function readnpz(path)
    data = np.load(path, allow_pickle=true)
    mesh = get(data, first(data.keys()))[1]
    points = get(mesh, "points")
    simplices = get(mesh, "simplices") .+ 1
    #attaches = get(mesh, "attaches") .+ 1
    return (points, simplices)
end

function hull(vertices, simplices; step=nothing)
    mesh = convex_hull(collect(eachcol(vertices)))
    v2 = .5sum(vertices .* vertices; dims=1)
    if isnothing(step)
        sample = randperm(size(vertices, 2))[1:10]
        v = vertices[:,sample]
        d2 = (transpose(v) * v) .- v2[:,sample] .- transpose(v2[:,sample])
        d = sqrt(-2minimum(d2))
        step = .01d
    end
    detailled_mesh = nothing
    pprev = uprev = nothing
    for p in mesh
        p = collect(p)
        uterminal = argmax(vec(transpose(p) * vertices - v2))
        if isnothing(detailled_mesh)
            detailled_mesh = [p]
        else
            pvec = p .- pprev
            pnorm = norm(pvec)
            punit = pvec ./ pnorm
            pproj = transpose(punit) * (vertices .- pprev)
            # find out first node among neighbors
            # ucandidates = Set(Iterators.flatten([simplex for simplex in eachrow(simplices) if uprev in simplex]))
            # ucandidates = [u for u in ucandidates if u != uprev]
            # vcandidates = vertices[:,ucandidates] .- pprev
            # vnorm = sqrt.(sum(vcandidates .* vcandidates; dims=1))
            # cosθ = pproj[:,ucandidates] ./ vnorm
            # u = ucandidates[argmax(vec(cosθ))]
            while true
                s = pproj[1,uprev] + step
                s < pnorm || break
                y = @. pprev + s * punit
                #
                edge, d2 = nothing, Inf
                for simplex in eachrow(simplices)
                    for (i, u) in enumerate(simplex[1:end-1])
                        α = pproj[1,u]
                        a = vertices[:,u]
                        for v in simplex[i+1:end]
                            β = pproj[1,v]
                            α′, β′ = minmax(α, β)
                            if α′ <= s < β′
                                b = vertices[:,v]
                                x = (β - s) / (β - α) * a + (s - α) / (β - α) * b
                                xy = y - x
                                d2′= sum(xy .* xy)
                                if d2′< d2
                                    d2 = d2′
                                    edge = α==α′ ? (u,v) : (v,u)
                                end
                            end
                        end
                    end
                end
                @assert !isnothing(edge)
                v, u = edge
                if v != uprev
                    push!(detailled_mesh, vertices[:,v])
                end
                if u != uterminal
                    push!(detailled_mesh, vertices[:,u])
                end
                uprev = u
            end
            push!(detailled_mesh, p)
        end
        pprev, uprev = p, uterminal
    end
    return detailled_mesh, step
end

function loadmeshes(datapath)
    step = nothing
    meshes = Dict{Int, Vector{Vector{Float64}}}()
    for mesh in readdir(datapath; join=true)
        endswith(mesh, ".npz") || continue
        idx = parse(Int, split(split(basename(mesh), '_')[2], '.')[1]) + 1
        vertices, simplices = readnpz(mesh)
        mesh, step = hull(vertices, simplices; step=step)
        meshes[idx] = mesh
    end
    return meshes
end

function autoplace(i, meshes)
    mesh = meshes[i]
    meshes = [VPolytope(m) for (j, m) in pairs(meshes) if i != j]
    cluster = Int[]
    clusters = [cluster]
    for (i, p) in enumerate(mesh)
        if any(mesh -> p in mesh, meshes)
            if !isempty(cluster)
                cluster = Int[]
                push!(clusters, cluster)
            end
        else
            push!(cluster, i)
        end
    end
    if isempty(cluster)
        pop!(clusters)
        if isempty(clusters)
            return mean(mesh)
        end
    end
    if 1 < length(clusters) && clusters[1][1] == 1 && clusters[end][end] == length(mesh)
        cluster = pop!(clusters)
        clusters[1] = cat(cluster, clusters[1]; dims=1)
    end
    cluster = clusters[argmax(length.(clusters))]
    return mean(mesh[cluster])
end

function boundingbox(meshes)
    xmin, xmax = extrema([p[1] for p in Iterators.flatten(values(meshes))])
    ymin, ymax = extrema([p[2] for p in Iterators.flatten(values(meshes))])
    return (xmin, ymin, xmax, ymax)
end

const labels = Dict(
    1 =>"VA3",
    2 =>"VT1",
    3 =>"DA3",
    4 =>"LL1",
    5 =>"DO3",
    6 =>"DA2",
    7 =>"DA1",
    8 =>"DT1",
    9 =>"DO1",
    10=>"DO2",
    11=>"DO4",
    12=>"LO1",
    13=>"DO5",
    14=>"VL1",
    15=>"VL2",
    16=>"VL3",
    17=>"LT4",
    18=>"SBM",
    19=>"LT1",
    20=>"LT2",
    21=>"LT3",
    22=>"VA2",
    23=>"VL4",
    24=>"VO1",
    25=>"VO2",
    26=>"VA1",
    27=>"VO3",
    28=>"VO6",
    29=>"VO5",
    30=>"VO4",
)

"""
    importmeshes(ouptutfile, inputdir)

Convert a specific dataset of .npz files into a .json file.
`inputdir` is expected to contain 30 *mesh_<index>.npz* files with *<index>* ranging from 0
to 29.

Muscle names were first matched with 10.1016/j.xpro.2020.100291 fig. 4 until multiple errors
were spotted in that article. Subsequently, 10.1371/journal.pbio.0000041 fig. 1 was used
instead as a reference, consistently with 10.7554/eLife.51781 fig. 1 / table 1.
"""
function importmeshes(outputfile="muscles.json", inputdir="meshes"; height=500, margin=2)
    # load npz files
    meshes = loadmeshes(inputdir)
    # rotate and scale
    xmin, ymin, xmax, ymax = boundingbox(meshes)
    scale = (height - 2margin) / (xmax - xmin)
    meshes = Dict(i => [[ymax-p[2], p[1]-xmin] .* scale .+ margin for p in mesh]
                  for (i, mesh) in pairs(meshes))
    # save to json
    open(outputfile, "w") do f
        write(f, "{\n")
        first = true
        for (i, mesh) in pairs(meshes)
            if first
                first = false
            else
                write(f, ",\n")
            end
            label = labels[i]
            labelpos = Graphics.svgpoints([autoplace(i, meshes)])
            write(f, "\t\"$label\": {\n    \"points\": \"$(Graphics.Polygon(mesh).svgpoints[])\",\n    \"label\": \"$labelpos\"\n  }")
        end
        write(f, "\n}")
    end
end

