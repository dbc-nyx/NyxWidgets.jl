# run this script with:
# $ python -q
# >>> exec(open('app.py').read())

# the below 3 lines should be removed if working in a Python-only project
import os
os.environ['PYTHON_JULIAPKG_PROJECT'] = os.getcwd()
os.environ['PYTHON_JULIAPKG_OFFLINE'] = 'yes'

from juliacall import Main as jl

install = r"""
using Pkg
Pkg.install(url="https://gitlab.com/dbc-nyx/NyxWidgets.jl")
Pkg.install("Bonito")
"""
# additionally, if working from a Python-only project,
# the following installation procedure is to be performed once:
#jl.seval(install)

code = r"""
using Bonito, NyxWidgets.Muscles

widget = nothing

app = App(; title="Meshes") do session
    global widget = MuscleWidget()
    Bonito.jsrender(session, Bonito.TailwindCSS)
    Bonito.jsrender(session, widget)
end

server = Server(app, "127.0.0.1", 9284)
show(server)
display(app)
"""

import random

def set_color(muscle, red, green, blue):
    assert 0 <= red and red <= 255
    assert 0 <= green and green <= 255
    assert 0 <= blue and blue <= 255
    jl.seval(f'''
    muscle = Muscles.polygon(widget["{muscle}"]).style
    muscle[]["fill"] = "rgb({red}, {green}, {blue})"
    muscle[]["fill-opacity"] = 1
    notify(muscle)
    ''')

def unset_color(muscle):
    jl.seval(f'''
    muscle = Muscles.polygon(widget["{muscle}"]).style
    empty!(muscle[])
    notify(muscle)
    ''')

if __name__ == "__main__":
    jl.seval(code)
    print(r"""
    Example script:
    >>> red, green, blue = [random.randint(0, 255) for _ in range(3)]
    >>> set_color('DO4', red, green, blue)
    """)

