# MuscleWidget example app

This sub-project involves installing PyCall and other dependencies that were useful to import meshes originally available in the .npz format.
Although the imported meshes are now provided by the Muscles module, this app still includes the import logic (see function `importmeshes` in the *import.jl* file.

For function `importmeshes` to work, run `make` first. A data directory named *meshes* is expected in the present directory and should contain the .npz files.

## Julia app

To set up the app, run `make` or `julia --project -e 'using Pkg; Pkg.develop(path="../..")'`.

The app is implemented by the *app.jl* file. Run it with:
```
./app.jl
```

## Python app

Running the installation procedure is required:
```
make
```

The app is implemented by the *app.py* file. Run it with:
```
source bin/activate
python -q
>>> exec(open('app.py').read())
```

At present, the web UI at http://localhost:9284 must be loaded manually, which may fail if done after the Python prompt is back.
