#!/bin/bash
#=
PROJECT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)

if [ -z "$JULIA" ]; then JULIA=julia; fi

exec "$JULIA" --project="$PROJECT_DIR" --startup-file=no --color=yes\
    -iq "${BASH_SOURCE[0]}" $@
=#

using Random
using Colors
using ColorSchemes
using Bonito
using Observables
using OrderedCollections
using NyxWidgets
using NyxWidgets.Players
using NyxWidgets.Graphics
using NyxWidgets.Muscles
using NyxWidgets.AnimatedLayers

include("sequences.jl") # defines roll_sequence and random_sequence

const muscle_activation_sequences = OrderedDict("roll" => roll_sequence,
                                                "rand1" => random_sequence(),
                                                "rand2" => random_sequence(),
                                                "rand3" => random_sequence(),
                                                "rand4" => random_sequence())

function drawframe(model, sequence::Observable, t)
    sequence = muscle_activation_sequences[sequence[]]
    drawframe(model, sequence, t)
end

function drawframe(model, sequence, t)
    @debug "Drawing frame" t=round(t; digits=4)
    for (muscle, mesh) in muscles(model)
        Graphics.hidden(mesh)[] && continue
        style = Muscles.polygon(mesh).style
        if haskey(sequence, muscle) && haskey(sequence[muscle], t)
            color = sequence[muscle][t]
            style[]["fill"] = color
            style[]["fill-opacity"] = 1
            notify(style)
        elseif !isempty(style[])
            empty!(style[])
            notify(style)
        end
    end
end

function clear(model)
    for (_, mesh) in muscles(model)
        Graphics.hidden(mesh)[] && continue
        style = Muscles.polygon(mesh).style
        if !isempty(style[])
            empty!(style[])
            notify(style)
        end
    end
end

mktimes(seq::Observable) = mktimes(seq[])

function mktimes(seq)
    seq = muscle_activation_sequences[seq]
    times = collect(Iterators.flatmap(keys, values(seq)))
    return timegrid(times)
end

function app(; title="Nyx prototype")
    # the code below is incorrectly indented as a result of extracting it from the App
    # block. Test apps now all define the NyxWidgets part of the view outside the app to
    # test the integrity of the view after multiple page reloads.

        #NyxWidgets.theme[] = nothing
        sequence = Observable("roll")
        allsegments = MultiSegmentMuscleWidget(; musclelabel=false)
        individualsegments = [MuscleWidget(; segment=segment.segment,
                                           side=segment.side,
                                           upsidedown=segment.side=="right",
                                           segmentlabel=true,
                                           style="width: 41%;")
                              for segment in segments(allsegments)]
        #
        times = mktimes(sequence)
        playbackspeeds = ["0.001", "0.01", "0.1", "0.25", "0.5", "1.0"]
        animation = LayeredAnimation(times, allsegments, individualsegments...;
                                     playbackspeeds=playbackspeeds,
                                     playerstyle="margin-top: 0.5rem;",
                                     layerstyle="border: solid 1px lightgray;")
        on(animation) do step
            if step == 0
                clear(activelayer(animation))
            else
                drawframe(activelayer(animation), sequence, timestamp(animation, step))
            end
        end
        on(Players.speed(animation)) do s
            @info "Playback speed" val=s
        end
        # TODO as manual tests: click on a muscle and check the two below listeners are
        # called, come back to the original view and trigger the second listerner only with
        # a click in the bounding box of a segment but not on a muscle.
        on(Muscles.click(allsegments)) do (muscle, _)
            @info "Click on muscle" name=muscle.name segment=muscle.segment side=muscle.side
            for segment in individualsegments
                if muscle.segment == segment.segment && muscle.side == segment.side
                    AnimatedLayers.open(animation, segment)
                    break
                end
            end
        end
        on(Muscles.click(MuscleWidget, allsegments)) do segment
            @info "Click on segment" name=segment.segment side=segment.side
            for segment′ in individualsegments
                if segment.segment == segment′.segment && segment.side == segment′.side
                    if segment′ !== activelayer(animation)
                        AnimatedLayers.open(animation, segment′)
                    end
                    break
                end
            end
        end
        foreach(individualsegments) do segment
            on(Muscles.click(segment)) do (muscle, _)
                @info "Click on muscle" name=muscle.name
            end
        end
        #
        on(sequence) do seq
            seqdata = muscle_activation_sequences[seq]
            @info "Switching to activation sequence" name=seq sequence=seqdata
            #rewind(player)
            timestamps(animation)[] = mktimes(seq)
        end

    App(; title=title) do session
        @info "New session" session=session.id
        onchange = js"(evt) => { $sequence.notify(evt.srcElement.value) }"
        sequenceselector = DOM.select(DOM.option(seq; value=seq)
                                      for seq in keys(muscle_activation_sequences);
                                      onchange=onchange)
        doc = DOM.div(sequenceselector, animation; style="width: 50%;")
        #Bonito.jsrender(session, Bonito.TailwindCSS)
        dom = Bonito.jsrender(session, doc)
        Bonito.evaljs(session, js"$sequenceselector.value = $(sequence[]);")
        return dom
    end
end

main(; port=9284, kwargs...) = Server(app(; kwargs...), "127.0.0.1", port)

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
