
include("../muscles/roll.jl")

const Color = String
const Sequence = Dict{Muscle, Dict{Float64, Color}}

const roll_sequence = begin
    tmax = maximum(Iterators.flatmap(keys, values(roll_muscle_activation_sequence))) / 200
    side = ("right", "left")
    t0 = (0., tmax)
    Dict(Muscle(muscle; side=side[i]) => Dict(t0[i] + t / 200 => color
                                              for (t, color) in pairs(sequence))
         for i in (1, 2) for (muscle, sequence) in pairs(roll_muscle_activation_sequence))
end

function random_sequence()
    sequences = Sequence()
    colorscheme = rand((ColorSchemes.leonardo, ColorSchemes.phase, ColorSchemes.thermal,
                        ColorSchemes.imola))
    nsteps = rand(50:80)
    for side in ("left", "right")
        for segment in 1:6
            muscles = collect(Muscles.__shortnames__)
            Random.shuffle!(muscles)
            nmuscles = rand(0:length(muscles))
            for label in muscles[1:nmuscles]
                muscle = Muscle(label, "A$segment", side)
                sequences[muscle] = sequence = Dict{Float64, Color}()
                start = stop = 0
                while stop - start < 3
                    start, stop = minmax(rand(1:nsteps), rand(1:nsteps))
                end
                for step in start:stop
                    color = "#" * hex(get(colorscheme, rand()))
                    sequence[step / 40] = color
                end
            end
        end
    end
    return sequences
end

Base.haskey(dict::Sequence, key::Muscle) = any(==(key), keys(dict))

function Base.getindex(dict::Sequence, key::Muscle)
    for (key′, val) in pairs(dict)
        if key == key′
            return val
        end
    end
    throw(KeyError(key))
end

