using NyxWidgets.Players
using Observables

@testset "Controller" begin
    times = .1:.1:1
    player1 = Player(times; timeout=.2)
    @test min(player1) == 1 && max(player1) == 10
    @test_throws AssertionError begin
        isplaying(player1)[] = false
    end
    Players.speed(player1)[] = 10
    stepaccum = Int[]
    timeaccum = eltype(times)[]
    on(player1) do step
        push!(stepaccum, step)
        push!(timeaccum, timestamp(player1))
    end
    # force precompilation
    timestep(player1)[] = 1
    # undo changes
    empty!(stepaccum)
    empty!(timeaccum)
    rewind(player1; silent=true)
    #
    t = 0.0
    function timeit(b)
        t = b ? time() : time() - t
    end
    on(timeit, isplaying(player1); priority=1)
    # force precompilation
    timeit(true)
    # undo changes
    t = 0.0
    # run test 1
    play(player1)
    wait(player1)
    @test stepaccum == 1:10
    @test timeaccum == times
    @test .09 < t < .901 # delays 1 to 9

    on(player1) do _
        # takes longer than the target time interval (=0.01)
        sleep(.011)
    end
    # force precompilation and undo changes
    timestep(player1)[] = 1
    rewind(player1; silent=true)
    t = 0.0
    empty!(stepaccum)
    empty!(timeaccum)
    # actual test
    play(player1)
    wait(player1)
    @test stepaccum == 1:2:10
    @test timeaccum == .1:.2:1.
    # t ≈ .094 without precompilation

    player2 = Player(times; timeout=.05)
    on(player2) do _
        sleep(1)
    end
    # test
    play(player2)
    sleep(.01)
    pause(player2)
    @test_warn "Killing task" begin
        sleep(.1)
    end
    @test Players.interrupted(player2)
    @test timestep(player2)[] == min(player2)

    player2 = Player(times; timeout=.05, quiet=true)
    on(player2) do _
        sleep(.01); error("callback error")
    end
    play(player2)
    exception = nothing
    try
        wait(player2)
    catch
        exception = current_exceptions(player2)[end][1]
    end
    @test exception == ErrorException("callback error")

    times = [100000.001, 100000.002, 100000.003, 100000.008, 100000.009, 100000.01]
    #julia> collect(100000.001:.001:nextfloat(100000.01))
    #10-element Vector{Float64}:
    # 100000.001
    # 100000.00200000001
    # 100000.003
    # 100000.004
    # 100000.005
    # 100000.00600000001
    # 100000.007
    # 100000.008
    # 100000.009
    # 100000.01000000001
    times′= timegrid(times)
    @test length(times′) == 10 && times′[1] == times[1] && times′[2] == times[2] &&
        times′[3] == times[3] && times′[8] == times[4] && times′[9] == times[5] &&
        times′[10] == times[6]
    player3 = Player(times′; quiet=true)
    Players.toggle(player3)
    # be nasty
    Players.speed(player3).val = 0
    exception = nothing
    try
        wait(player3)
    catch
        exception = current_exceptions(player3)[end][1]
    end
    @test exception == DomainError(0.0, "negative or null playback speed")
end
